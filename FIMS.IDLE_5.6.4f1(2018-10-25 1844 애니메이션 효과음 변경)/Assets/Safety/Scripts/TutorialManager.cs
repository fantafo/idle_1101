﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour {

    public GameObject[] _targets;
    public GameObject[] _effects;    
    public GameObject _toLoginBtn;

    private int _curIdx = 0;

    private Vector3[] _myScale = new Vector3[2];

    // Use this for initialization
    void Start()
    {
        //_myScale[0] = _gazeBtn.transform.localScale;
        //_myScale[1] = _toLoginBtn.transform.localScale;
        //_gazeBtn.transform.localScale = new Vector3(0, 0, 0);
        //_toLoginBtn.transform.localScale = new Vector3(0, 0, 0);        
        _toLoginBtn.SetActive(false);
        for (int i = 0; i < _effects.Length; i++)
        {
            _effects[i].SetActive(false);
            _targets[i].SetActive(false);
        }
        _targets[0].SetActive(true);
    }
	
    public void OnClickTarget()
    {
        if (_curIdx < 2 )
        {
            _targets[_curIdx].SetActive(false);
            _effects[_curIdx].SetActive(true);
            _curIdx++;
            _targets[_curIdx].SetActive(true);            
        }
        else
        {
            _targets[_curIdx].SetActive(false);
            _effects[_curIdx].SetActive(true);            

            //_gazeBtn.SetActive(true);
            StartCoroutine(ClickTarget());

            //_gazeBtn.transform.localScale = _myScale[0];
            //_toLoginBtn.transform.localScale = _myScale[1];
        }

    }

    private IEnumerator ClickTarget()
    {
        yield return new WaitForSeconds(1.5f);

        _toLoginBtn.SetActive(true);
    }

}
