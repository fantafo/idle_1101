﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveArrow : MonoBehaviour {

	// Use this for initialization
	void Start () {
        iTween.MoveBy(gameObject, iTween.Hash("x", -0.02, "easeType", "easeInOutExpo", "loopType", "loop","time", 0.5, "delay", 0.07));
    }
	
}
