﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ControllerConnectionVisualizer : MonoBehaviour
{
    Text text;
    bool isConnected;

    private void Awake()
    {
        text = GetComponent<Text>();
        Setup();
    }

    void Update()
    {
        if (isConnected != VRInput.isPresentController)
        {
            Setup();
        }
    }

    void Setup()
    {
        isConnected = VRInput.isPresentController;
        if (isConnected)
        {
            text.text = "(Connected)";
        }
        else
        {
            text.text = "(Scanning..)";
        }
    }
}
