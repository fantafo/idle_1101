﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class AllocChecker : MonoBehaviour
{
    float time = 0;

    private void Update()
    {
        time += Time.fixedDeltaTime;
        if(time > 0.5f)
        {
            time = 0;

            if (!FimsCommonData.IsAllocated)
            {
                AppExiter.Exit("AllocChecker.Start.Quit");
            }
        }
    }
}