﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
#if UNITY_EDITOR
    const string ROOT = "";
#else
    const string ROOT = "/sdcard/Fantafo/";
#endif
    const string SCORE_PATH = ROOT + "score.properties";

    public Text txt;

    float scoreTime;

    private void OnApplicationPause(bool pause)
    {
        if(pause)
        {
            OnDisable();
        }
        else
        {
            OnEnable();
        }
    }
    private void OnEnable()
    {
        bool useScore = File.Exists(SCORE_PATH);
        txt.gameObject.SetActive(useScore);
        if(useScore)
        {
            scoreTime = Time.time;
            Properties p = new Properties(SCORE_PATH);
            string name = p.GetString("name", "-");
            int score = p.GetInt("score", 0);

            string reward;
            if (score > 200000)
            {
                reward = "20만점을 넘어서 인형을 획득하셨습니다!\n카운터에서 받아주세요!";
            }
            else if (score > 150000)
            {
                reward = "15만점이 넘어서 아메리카노 기프티콘을 획득하셨습니다!\n카운터에서 받아주세요!";
            }
            else if (score > 100000)
            {
                reward = "10만점이 넘어서 VR방 1500원 할인쿠폰을 받으셨습니다!\n다음에 또 이용해주세요!";
            }
            else if (score > 50000)
            {
                reward = "5만점이 넘어서 VR방 1000원 할인쿠폰을 받으셨습니다!\n다음에 또 이용해주세요!";
            }
            else if (score > 10000)
            {
                reward = "VR방 500원 할인쿠폰을 받으셨습니다!\n다음에 또 이용해주세요!";
            }
            else
            {
                reward = "즐거우셨나요?\n다음에 또 이용해주세요!";
            }
        

            txt.text = string.Format("{0}님의 점수는 {1:n0}입니다!\n\n{2}", name, score, reward);

        }
    }
    private void OnDisable()
    {
        if(scoreTime > 10)
        {
            File.Delete(SCORE_PATH);
        }
    }
}
