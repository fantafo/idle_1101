﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// PlayerInfo의 위치를 자동으로 UserLocation에 배치해준다.
    /// 배치 대상은 Player의 방에 접속한 순번으로 배치된다.
    /// </summary>
    [RequireComponent(typeof(PlayerInfo))]
    public class AutoLocating : SMonoBehaviour
    {
        PlayerInfo info;
        UserLocation beforeLocate;

        private void Awake()
        {
            info = GetComponent<PlayerInfo>();
        }
        private void Update()
        {
            if(info.instance != null && UserLocationManager.main)
            {
                var locate = UserLocationManager.main.locates.TryGet(info.instance.roomIndex);
                if(locate != null)
                {
                    if(locate != beforeLocate || locate.transform.hasChanged)
                    {
                        info.transform.position = locate.transform.position;
                        info.transform.rotation = locate.transform.rotation;

                        locate.transform.hasChanged = false;
                        beforeLocate = locate;
                    }
                }
            }
        }
    }
}