﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// 카메라의 최상위에 색상을 입혀 페이즈 인/아웃 효과를 구현합니다.
/// </summary>
public class ScreenFader : MonoBehaviour
{
    readonly static WaitForEndOfFrame WaitEOF = new WaitForEndOfFrame();

    public static ScreenFader main;

    /********************************************************************************/
    /*                                                                              */
    /*                               Variables                                      */
    /*                                                                              */
    /********************************************************************************/

    [Tooltip("페이드 속도")]
    public float _defaultDuration = 1.0f;
    public bool _fadeInLoaded = true;
    public bool _useUnscaledTime = true;

    [Tooltip("페이드 대상")]
    [SerializeField]
    private ColorRenderer[] _renderers;
    public Material _fadeMaterial;
    public Color _color = new Color(0, 0, 0, 1.0f);

    public bool IsFading;

    private Coroutine _routine;


    public ColorRenderer[] Renderers
    {
        get { return _renderers; }
        set
        {
            _renderers = value;
            if (_renderers != null)
            {
                foreach (ColorRenderer rend in _renderers)
                {
                    rend._colorMaterial = _fadeMaterial;
                }
            }
        }
    }
    

    public float TimeDelta
    {
        get
        {
            return _useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
        }
    }

    /********************************************************************************/
    /*                                                                              */
    /*                             Life Cycles                                      */
    /*                                                                              */
    /********************************************************************************/

    void Awake()
    {
        if (main == null)
        {
            main = this;
        }

        if (_fadeMaterial == null)
            _fadeMaterial = new Material(Shader.Find("Sprites/Default"));

        if (_renderers != null)
        {
            foreach (ColorRenderer rend in _renderers)
            {
                rend._colorMaterial = _fadeMaterial;
            }
        }

        SceneManager.sceneLoaded += OnLevelLoaded;
    }


    void OnLevelLoaded(Scene scene, LoadSceneMode mode)
    {
        if (_fadeInLoaded)
            FadeIn();
    }

    void OnDestroy()
    {
        if (_fadeMaterial != null)
            Destroy(_fadeMaterial);
        SceneManager.sceneLoaded -= OnLevelLoaded;

        if (main == this)
            main = null;
        if (main == null)
            main = GameObject.FindObjectOfType<ScreenFader>();
    }



    /********************************************************************************/
    /*                                                                              */
    /*                                  Coroutine                                   */
    /*                                                                              */
    /********************************************************************************/

    IEnumerator FadeInRoutine(float fadeTime, Action callback)
    {
        IsFading = true;
        EnableColorRenderer(true);

        float elapsedTime = 0.0f;
        SetAlpha(1f);
        while (elapsedTime < fadeTime)
        {
            yield return WaitEOF;
            elapsedTime += TimeDelta;
            SetAlpha(1f - Mathf.Clamp01(elapsedTime / fadeTime));
        }
        SetAlpha(0f);

        yield return null;
        EnableColorRenderer(false);
        if (callback != null)
            callback();
        IsFading = false;
    }
    IEnumerator FadeOutRoutine(float fadeTime, Action callback)
    {
        IsFading = true;
        EnableColorRenderer(true);

        float elapsedTime = 0.0f;
        SetAlpha(0f);
        while (elapsedTime < fadeTime)
        {
            yield return WaitEOF;
            elapsedTime += TimeDelta;
            SetAlpha(Mathf.Clamp01(elapsedTime / fadeTime));
        }
        SetAlpha(1f);

        yield return null;
        EnableColorRenderer(false);
        if (callback != null)
            callback();
        IsFading = false;
    }

    void SetAlpha(float alpha)
    {
        _color.a = alpha;
        _fadeMaterial.color = _color;
    }

    void EnableColorRenderer(bool enable)
    {
        foreach (ColorRenderer r in _renderers)
        {
            r.enabled = enabled;
        }
    }

    /********************************************************************************/
    /*                                                                              */
    /*                                  Methods                                     */
    /*                                                                              */
    /********************************************************************************/

    /// <summary>
    /// 화면에서 점점 색이 없어지며, 원래의 카메라 화면이 나타난다.
    /// 페이드하여 장면에 들어간다는 뜻
    /// </summary>
    [Button]
    public void FadeIn()
    {
        FadeIn(_defaultDuration);
    }
    public void FadeIn(Action action)
    {
        FadeIn(_defaultDuration, action);
    }
    public void FadeIn(float duration, Action action = null)
    {
        try
        {
            if (_routine != null)
                StopCoroutine(_routine);
        }catch(Exception e)
        {
        }
        _routine = StartCoroutine(FadeInRoutine(duration, action));
    }
    /// <summary>
    /// 화면에서 점점 색이 생기며 단색으로 체워진다.
    /// 페이드하여 장면에서 나온다는 뜻
    /// </summary>
    [Button]
    public void FadeOut()
    {
        FadeOut(_defaultDuration);
    }
    public void FadeOut(Action action)
    {
        FadeOut(_defaultDuration, action);
    }
    public void FadeOut(float duration, Action action = null)
    {
        try
        {
            if (_routine != null)
                StopCoroutine(_routine);
        }
        catch (Exception e)
        {
        }
        _routine = StartCoroutine(FadeOutRoutine(duration, action));
    }


    public void FadeOutIn(float duration, Action blackAction = null, Action releaseAction = null)
    {
        FadeOut(duration, () =>
        {
            if (blackAction != null)
                blackAction();

            FadeIn(duration, releaseAction);
        });
    }

}
