﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace FTF
{
    /// <summary>
    /// 클라이언트에게 유일한 방의 정보를 가리킨다.
    /// 현재 접속해있는 방을 가리키기도 한다.
    /// </summary>
    public class RoomInfo : SMonoBehaviour, IReceivePacket
    {
        /// <summary>
        /// 현재 접속해있는 방의 정보를 가리킨다.
        /// </summary>
        public static RoomInfo main { get; private set; }


        //================================================================================================
        //
        //                                       Member Variables                                  
        //
        //================================================================================================

        public long num; // 서버에서 구별하는 방의 번호
        public string roomName; // 방의 이름
        public int maxUser; // 방에 접속가능한 최대 인원 수
        public int mapID; // 방에서 사용할 맵 번호

        public int currentUserCount; // 실제 접속한 유저의 수. (목록에만 있고 접속하지 않은 유저와는 다르다)
        public PlayerInfo[] players; // 플레이어 리스트. (접속하지 않았거나 할당만 된 대상도 포함된 리스트)

        public UnityEvent onRecevieRoomInfo; // 룸 정보가 갱신되면 호출된다.
        public UnityEvent onLoadStart; // 룸의 세션이 시작되면 호출된다.


        //================================================================================================
        //
        //                                       Unity LifeCycle
        //
        //================================================================================================

        private void Awake()
        {
            if (main)
            {
                Destroy(main);
                return;
            }
            main = this;
        }


        //================================================================================================
        //
        //                                          Network
        //
        //================================================================================================

        public void OnReceivePacket(ServerOpcode code, BinaryReader reader)
        {
            switch (code)
            {
                //=====================================================
                // 룸 정보를 읽어옵니다.
                case ServerOpcode.Room_Info:
                    {
                        num = reader.ReadL();
                        roomName = reader.ReadS();
                        maxUser = reader.ReadC();
                        mapID = reader.ReadH();

                        if (players == null)
                            players = new PlayerInfo[maxUser];
                        else if (players.Length != maxUser)
                            Array.Resize(ref players, maxUser);

                        const int SKIPSIZE = (3 + 4 + 3 + 4 + 3) * 4 + 1;
                        byte[] skipData = BytePool.Take(SKIPSIZE);

                        int userCount = reader.ReadC();
                        currentUserCount = 0;
                        for (int i = 0; i < players.Length; i++)
                        {
                            // 접속된 대상만 읽어온다
                            if (reader.ReadB())
                            {
                                currentUserCount++;

                                int instanceID = -1;
                                if (i < userCount)
                                    instanceID = reader.ReadD();

                                PlayerInstance player = null;
                                if (instanceID != -1)
                                    player = PlayerManager.GetPlayer(instanceID);

                                // 플레이어가 존재하다가 사라졌을경우
                                if (player == null)
                                {
                                    if (players[i] != null)
                                    {
                                        players[i].gameObject.Destroy();
                                    }

                                    // instanceID가 -1일경우는 패킷이 없는경우
                                    if (instanceID != -1)
                                    {
                                        PlayerManager.GetPlayer(instanceID, p => { });
                                        reader.Read(skipData, 0, SKIPSIZE);
                                    }
                                }
                                else if(PlayerInfoManager.main != null)
                                {
                                    if (players[i] == null)
                                    {
                                        players[i] = PlayerInfoManager.main.TakePlayer();
                                    }
                                    if (players[i].instance != player)
                                    {
                                        // 플레이어가 이미 할당 돼 있는 상태라면?
                                        if (player.info != null && player.info.instance == player)
                                        {
                                            // 만약 리스트에 할당된 PlayerInfo가 있다면 각각의 위치를 바꿔준다.
                                            int indexOf = Array.IndexOf(players, player.info);
                                            if (indexOf != -1)
                                            {
                                                var temp = players[i];
                                                players[i] = players[indexOf];
                                                players[indexOf] = temp;

                                                continue;
                                            }
                                        }

                                        // index 위치의 플레이어에게 player를 할당한다.
                                        players[i].instance = player;
                                        player.info = players[i];
                                        player.info.gameObject.SetActive(true);
                                    }

                                    if (player.info != null && player.info.model != null)
                                    {
                                        player.roomIndex = i;
                                        player.info.model.headPosition = reader.ReadNormal();
                                        player.info.model.headRotation = Quaternion.Euler(reader.ReadEuler24());
                                        player.info.model.handPosition = reader.ReadNormal();
                                        player.info.model.handRotation = Quaternion.Euler(reader.ReadEuler24());
                                        player.info.model.IsReticleVisible = reader.ReadB();
                                        player.info.model.reticlePosition = reader.ReadVector3();

                                        if (player.info.model.headRotation.w == 0)
                                            player.info.model.headRotation = Quaternion.identity;
                                        if (player.info.model.handRotation.w == 0)
                                            player.info.model.handRotation = Quaternion.identity;
                                    }
                                    else
                                    {
                                        reader.Read(skipData, 0, SKIPSIZE);
                                    }
                                }
                            }
                        }

                        if (onRecevieRoomInfo != null)
                            onRecevieRoomInfo.Invoke();

                        BytePool.Release(skipData);

                        if (Networker.State < NetworkState.Room)
                            Networker.State = NetworkState.Room;
                    }
                    break;

                //=====================================================
                // 방이 시작됐기때문에 읽기 시작하라고 알려줍니다.
                case ServerOpcode.Room_LoadStart:
                    {
                        Networker.State = NetworkState.Loading;
                        if (onLoadStart != null)
                        {
                            onLoadStart.Invoke();
                        }
                    }
                    break;

                case ServerOpcode.Room_Exit:
                    {

                    }
                    break;
            }
        }
    }
}
