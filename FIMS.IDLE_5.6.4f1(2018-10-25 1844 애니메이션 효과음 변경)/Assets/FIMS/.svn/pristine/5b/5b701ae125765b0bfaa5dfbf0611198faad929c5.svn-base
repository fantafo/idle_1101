﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;
using SFramework.TweenAction;

public partial class STween
{
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                         Play Component                      //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState PlayBase<Action>(float duration)
        where Action : BaseTweenAction
    {
        var module = NewAction<Action>();
        module.Init();

        var state = NewState(duration);
        state.SetAction(module);
        s_tweens.Add(state);

        return state;
    }


    public static STweenState Play<Action, Comp, Var, Cur>(Comp target, Var to, float duration)
        where Action : ComponentAction<Comp, Var, Cur>
    {
        if (target == null)
            throw new STweenException("Tween Component Target is null. : " + typeof(Comp).Name);

        var module = NewAction<Action>();

        module.SetComponent(target);
        module.SetTo(to);
        module.Init();

        var state = NewState(duration);
        state.SetAction(module);
        s_tweens.Add(state);

        return state;
    }
    public static STweenState Play<Action, Comp, Var, Cur>(Comp target, Var from, Var to, float duration)
        where Action : ComponentAction<Comp, Var, Cur>
    {
        if (target == null)
            throw new STweenException("Tween Component Target is null. : " + typeof(Comp).Name);

        var module = NewAction<Action>();

        module.SetComponent(target);
        module.SetTo(to);
        module.Init();
        module.SetFrom(from);

        var state = NewState(duration);
        state.SetAction(module);
        s_tweens.Add(state);

        return state;
    }

    public static STweenState Play<Action, Comp, Var>(Comp target, float to, float duration)
        where Action : ComponentAction<Comp, float, Var>
    {
        return Play<Action, Comp, float, Var>(target, to, duration);
    }
    public static STweenState Play<Action, Comp, Var>(Comp target, Vector2 to, float duration)
        where Action : ComponentAction<Comp, Vector2, Var>
    {
        return Play<Action, Comp, Vector2, Var>(target, to, duration);
    }
    public static STweenState Play<Action, Comp, Var>(Comp target, Vector3 to, float duration)
        where Action : ComponentAction<Comp, Vector3, Var>
    {
        return Play<Action, Comp, Vector3, Var>(target, to, duration);
    }
    public static STweenState Play<Action, Comp, Var>(Comp target, Vector4 to, float duration)
        where Action : ComponentAction<Comp, Vector4, Var>
    {
        return Play<Action, Comp, Vector4, Var>(target, to, duration);
    }
    public static STweenState Play<Action, Comp, Var>(Comp target, Quaternion to, float duration)
        where Action : ComponentAction<Comp, Quaternion, Var>
    {
        return Play<Action, Comp, Quaternion, Var>(target, to, duration);
    }

    public static STweenState Play<Action, Comp, Var>(Comp target, float from, float to, float duration)
        where Action : ComponentAction<Comp, float, Var>
    {
        return Play<Action, Comp, float, Var>(target, from, to, duration);
    }
    public static STweenState Play<Action, Comp, Var>(Comp target, Vector2 from, Vector2 to, float duration)
        where Action : ComponentAction<Comp, Vector2, Var>
    {
        return Play<Action, Comp, Vector2, Var>(target, from, to, duration);
    }
    public static STweenState Play<Action, Comp, Var>(Comp target, Vector3 from, Vector3 to, float duration)
        where Action : ComponentAction<Comp, Vector3, Var>
    {
        return Play<Action, Comp, Vector3, Var>(target, from, to, duration);
    }
    public static STweenState Play<Action, Comp, Var>(Comp target, Vector4 from, Vector4 to, float duration)
        where Action : ComponentAction<Comp, Vector4, Var>
    {
        return Play<Action, Comp, Vector4, Var>(target, from, to, duration);
    }
    public static STweenState Play<Action, Comp, Var>(Comp target, Quaternion from, Quaternion to, float duration)
        where Action : ComponentAction<Comp, Quaternion, Var>
    {
        return Play<Action, Comp, Quaternion, Var>(target, from, to, duration);
    }


    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                         Play Actions                        //
    //                                                             //
    /////////////////////////////////////////////////////////////////
    public static STweenState Play<Action, Comp, Variable>(Variable from, Variable to, float duration, Comp comp)
        where Action : ComponentAction<Comp, Variable>
    {
        if (comp == null)
            throw new STweenException("Tween Component Target is null. : " + typeof(Comp).Name);

        var module = NewAction<Action>();

        module.SetComponent(comp);
        module.SetTo(to);
        module.Init();
        module.SetFrom(from);

        var state = NewState(duration);
        state.SetAction(module);
        s_tweens.Add(state);

        return state;
    }

    public static STweenState Play<Action, Comp, Variable>(Comp comp, Variable from, Variable to, float duration, System.Action<Comp, Variable> action)
        where Action : BaseActionValueObject<Comp, Variable>
    {
        if (action == null)
            throw new STweenException("Tween Component Target is null. : " + typeof(Comp).Name);

        var module = NewAction<Action>();

        module.SetArgument(comp);
        module.SetComponent(action);
        module.SetTo(to);
        module.Init();
        module.SetFrom(from);

        var state = NewState(duration);
        state.SetAction(module);
        s_tweens.Add(state);

        return state;
    }
}
