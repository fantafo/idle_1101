﻿namespace FTF
{
    /// <summary>
    /// 오큘러스, 데이드림 등의 플렛폼에 맞는 CameraRig을 가리킨다.
    /// 이것은 씬에 유일하게 활성화가 돼 있어야하며, 이것은 MainPlayerController.main.cameraRig으로 획득할 수 있다.
    /// </summary>
    public class MainPlayerCameraRig : SMonoBehaviour
    {
        public GvrReticlePointer reticlePointer;
        public bool oculus;

        private void OnEnable()
        {
            MainPlayerController.main.cameraRig = this;
        }
        private void OnDisable()
        {
            MainPlayerController.main.cameraRig = this;
        }

        private void Update()
        {
            if (reticlePointer != null)
            {
                if(oculus)
                {

                }
                else
                {

                }
                reticlePointer.gameObject.SetActive(!VRInput.isPresentController);
            }
        }
    }
}