﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class AppExiter : SMonoBehaviour
{
    static bool shouldExit;
    static string exitMessage;
    public static void Exit(string reason = "unknown")
    {
        if(!shouldExit)
        {
            shouldExit = true;
            exitMessage = reason;
        }
        SLog.Error("AppExiter", "Exit - " + reason);
    }

    int startFrame;

    private void Update()
    {
        if(shouldExit)
        {
            SLog.Error("AppExiter", "Force Quit");
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        if(focus)
        {
            startFrame = Time.frameCount;
        }
        else
        {
            startFrame = int.MaxValue;
        }
    }
}
