﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.VR;

/// <summary>
/// 카드보드, 데이드림, 오큘러스 등의 Input정보를 동일한 형태로 묶어준다.
/// 각 플랫폼에 맞게 카메라, 컨트롤러를 활성화를 위한 CameraRig을 제어한다.
/// </summary>
public class VRInput : SMonoBehaviour
{
    static Platform platform;
    public static bool isOculus { get { return platform is OculusPlatform; } }
    public static bool isDaydream { get { return platform is DaydreamPlatform; } }
    public static bool isCardboard { get { return platform is GooglePlatform; } }

    public static bool isPresentHMD { get { return platform != null ? platform.isPresentHMD : false; } }
    public static bool isPresentController { get { return platform != null ? platform.isPresentController : false; } }
    public static Vector3 headPosition { get { return platform != null ? platform.headPosition : Vector3.zero; } }
    public static Quaternion headOrientation { get { return platform != null ? platform.headOrientation : Quaternion.identity; } }
    public static Vector3 controllerPosition { get { return platform != null ? platform.controllerPosition : Vector3.zero; } }
    public static Quaternion controllerOrientation { get { return platform != null ? platform.controllerOrientation : Quaternion.identity; } }
#if UNITY_EDITOR
    public static bool IsTouching { get { return (platform != null ? platform.IsTouching : false) | (Input.GetButton("Horizontal") || Input.GetButton("Vertical")); } }
    public static bool TouchDown {  get { return (platform != null ? platform.TouchDown : false)  | (Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical")); } }
    public static bool TouchUp {    get { return (platform != null ? platform.TouchUp : false)    | (Input.GetButtonUp("Horizontal") || Input.GetButtonUp("Vertical")); } }
    /// <summary>
    /// 터치 좌표계가 0 ~ 1 사이의 값을 나타냅니다.
    /// </summary>
    public static Vector2 TouchPos { get { return (Input.GetButton("Horizontal") || Input.GetButton("Vertical")) ? new Vector2((1+Input.GetAxis("Horizontal"))*.5f, (1+Input.GetAxis("Vertical"))*.5f) : (platform != null ? platform.TouchPos : new Vector2(0.5f, 0.5f)); } }
    /// <summary>
    /// 터지 좌표계가 -1 ~ +1 사이의 값을 나타냅니다.
    /// </summary>
    public static Vector2 TouchDir { get { return (Input.GetButton("Horizontal") || Input.GetButton("Vertical")) ? new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) : (platform != null ? platform.TouchDir : Vector2.zero); } }
#else
    public static bool IsTouching { get { return platform != null ? platform.IsTouching : false; } }
    public static bool TouchDown { get { return platform != null ? platform.TouchDown : false; } }
    public static bool TouchUp { get { return platform != null ? platform.TouchUp : false; } }
    /// <summary>
    /// 터치 좌표계가 0 ~ 1 사이의 값을 나타냅니다.
    /// </summary>
    public static Vector2 TouchPos { get { return platform != null ? platform.TouchPos : new Vector2(0.5f, 0.5f); } }
    /// <summary>
    /// 터지 좌표계가 -1 ~ +1 사이의 값을 나타냅니다.
    /// </summary>
    public static Vector2 TouchDir { get { return platform != null ? platform.TouchDir : Vector2.zero; } }
#endif
    public static bool Trigger { get { return platform != null ? platform.Trigger : false; } }
    public static bool TriggerDown { get { return platform != null ? platform.TriggerDown : false; } }
    public static bool TriggerUp { get { return platform != null ? platform.TriggerUp : false; } }
    public static bool ClickButton { get { return platform != null ? platform.ClickButton : false; } }
    public static bool ClickButtonDown { get { return platform != null ? platform.ClickButtonDown : false; } }
    public static bool ClickButtonUp { get { return platform != null ? platform.ClickButtonUp : false; } }
    public static bool BackButton { get { return platform != null ? platform.BackButton : false; } }
    //public static bool Recentered { get { return platform != null ? platform.Recentered : false; } }


    [System.Serializable]
    public class VRPlatform
    {
        public string deviceName;
        public GameObject[] activeTargets;
    }
    public VRPlatform[] platforms;
#if UNITY_EDITOR
    public string EDITOR_DEVICE_NAME;
    private void Reset()
    {
        platforms = new VRPlatform[]
        {
            new VRPlatform
            {
                deviceName = "Oculus"
            },
            new VRPlatform
            {
                deviceName = "Daydream"
            }
        };
        EDITOR_DEVICE_NAME = "Daydream";
    }
#endif

    private void Awake()
    {
        string deviceName = VRSettings.loadedDeviceName;
#if UNITY_EDITOR

        deviceName = EDITOR_DEVICE_NAME;
#endif
        Debug.Log("VR Input device is '" + deviceName + "'");

        // deactives
        for (int i = 0; i < platforms.Length; i++)
        {
            VRPlatform p = platforms[i];
            bool active = (deviceName.Equals(p.deviceName, StringComparison.OrdinalIgnoreCase));
            if (active)
                deviceName = p.deviceName;

            if (!active)
            {
                for (int j = 0; j < p.activeTargets.Length; j++)
                {
                    p.activeTargets[j].SetActive(active);
                }
            }
        }
        // actvie
        for (int i = 0; i < platforms.Length; i++)
        {
            VRPlatform p = platforms[i];
            bool active = (deviceName.Equals(p.deviceName, StringComparison.OrdinalIgnoreCase));
            if (active)
            {
                deviceName = p.deviceName;
                for (int j = 0; j < p.activeTargets.Length; j++)
                {
                    p.activeTargets[j].SetActive(true);
                }
            }
        }

        switch (deviceName)
        {
            case "Daydream":
                platform = new DaydreamPlatform();
                break;
            case "Oculus":
                platform = new OculusPlatform();
                break;
            default:
                platform = new GooglePlatform();
                break;
        }
        Debug.Log("VR Input selected '" + platform.GetType().Name + "'");
        platform.Start();
    }

    private void OnEnable()
    {
        StartCoroutine(EndOfFrame());
    }

    private void Update()
    {
        platform.Update();
    }

    private IEnumerator EndOfFrame()
    {
        while (true)
        {
            yield return Waits.EndOfFrame;
            platform.UpdateEOF();
        }
    }

    public class Platform
    {
        public virtual void Start() { }
        public virtual void Update() { }
        public virtual void UpdateEOF() { }

        public virtual bool isPresentHMD { get { return false; } }
        public virtual bool isPresentController { get { return false; } }

        public virtual Vector3 headPosition { get { return InputTracking.GetLocalPosition(VRNode.Head); } }
        public virtual Quaternion headOrientation { get { return InputTracking.GetLocalRotation(VRNode.Head); } }
        public virtual Vector3 controllerPosition { get { return Vector3.zero; } }
        public virtual Quaternion controllerOrientation { get { return Quaternion.identity; } }

        public virtual bool IsTouching { get { return false; } }
        public virtual bool TouchDown { get { return false; } }
        public virtual bool TouchUp { get { return false; } }
        public virtual Vector2 TouchPos { get { return new Vector2(0.5f, 0.5f); } }
        public virtual Vector2 TouchDir { get { return Vector2.zero; } }

        public virtual bool Trigger { get { return false; } }
        public virtual bool TriggerDown { get { return false; } }
        public virtual bool TriggerUp { get { return false; } }

        public virtual bool ClickButton { get { return false; } }
        public virtual bool ClickButtonDown { get { return false; } }
        public virtual bool ClickButtonUp { get { return false; } }

        public virtual bool BackButton { get { return false; } }
    }
    public class OculusPlatform : Platform
    {
        OVRInput.Controller controllerType = OVRInput.Controller.RTrackedRemote;

        public override void Update()
        {
            controllerType = OVRInput.GetConnectedControllers() & (OVRInput.Controller.LTrackedRemote | OVRInput.Controller.RTrackedRemote);
        }

        public override bool isPresentHMD { get { return OVRManager.isHmdPresent; } }
        public override bool isPresentController { get { return (OVRInput.GetActiveController() & (OVRInput.Controller.LTrackedRemote | OVRInput.Controller.RTrackedRemote)) != 0; } }

        public override Vector3 controllerPosition { get { return OVRInput.GetLocalControllerPosition(controllerType); } }
        public override Quaternion controllerOrientation { get { return OVRInput.GetLocalControllerRotation(controllerType); } }

        public override bool IsTouching { get { return OVRInput.Get(OVRInput.Touch.PrimaryTouchpad); } }
        public override bool TouchDown { get { return OVRInput.GetDown(OVRInput.Touch.PrimaryTouchpad); } }
        public override bool TouchUp { get { return OVRInput.GetUp(OVRInput.Touch.PrimaryTouchpad); } }
        public override Vector2 TouchPos { get { return (ReverseY(OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad)) + Vector2.one) * 0.5f; } }
        public override Vector2 TouchDir { get { return ReverseY(OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad)); } }
        Vector2 ReverseY(Vector2 v) { v.y *= -1; return v; }

        public override bool Trigger { get { return OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger); } }
        public override bool TriggerDown { get { return OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger); } }
        public override bool TriggerUp { get { return OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger); } }

        public override bool ClickButton { get { return OVRInput.Get(OVRInput.Button.PrimaryTouchpad | OVRInput.Button.One); } }
        public override bool ClickButtonDown { get { return OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad | OVRInput.Button.One); } }
        public override bool ClickButtonUp { get { return OVRInput.GetUp(OVRInput.Button.PrimaryTouchpad | OVRInput.Button.One); } }

        public override bool BackButton { get { return OVRInput.GetDown(OVRInput.Button.Back); } }
    }
    public class GooglePlatform : Platform
    {
        internal static bool ReticleClickButton;
        internal static bool ReticleClickButtonUp;
        internal static bool ReticleClickButtonDown;

#if UNITY_EDITOR
        public override Vector3 headPosition { get { return Camera.main.transform.localPosition; } }
        public override Quaternion headOrientation { get { return Camera.main.transform.localRotation; } }
#endif
        public override bool isPresentHMD { get { return true; } }

        public override bool Trigger { get { return ReticleClickButton; } }
        public override bool TriggerDown { get { return ReticleClickButtonDown; } }
        public override bool TriggerUp { get { return ReticleClickButtonUp; } }

        public override bool ClickButton { get { return ReticleClickButton; } }
        public override bool ClickButtonDown { get { return ReticleClickButtonDown; } }
        public override bool ClickButtonUp { get { return ReticleClickButtonUp; } }

        public override bool BackButton { get { return GvrControllerInput.AppButtonDown; } }
    }

    public class DaydreamPlatform : GooglePlatform
    {
#if UNITY_HAS_GOOGLEVR && UNITY_ANDROID
        public override bool isPresentController { get { return GvrControllerInput.State == GvrConnectionState.Connected; } }

        public override Vector3 controllerPosition { get { return FTF.FTFPointerBridge.Instance != null ? FTF.FTFPointerBridge.Instance.position : Vector3.down; } }
        public override Quaternion controllerOrientation { get { return FTF.FTFPointerBridge.Instance != null ? FTF.FTFPointerBridge.Instance.rotation : Quaternion.identity; } }

        public override bool IsTouching { get { return GvrControllerInput.IsTouching; } }
        public override bool TouchDown { get { return GvrControllerInput.TouchDown; } }
        public override bool TouchUp { get { return GvrControllerInput.TouchUp; } }
        public override Vector2 TouchPos { get { return GvrControllerInput.TouchPos; } }
        public override Vector2 TouchDir { get { return (GvrControllerInput.TouchPos * 2) - Vector2.one; } }

        public override bool Trigger { get { return GvrControllerInput.ClickButton; } }
        public override bool TriggerDown { get { return GvrControllerInput.ClickButtonDown; } }
        public override bool TriggerUp { get { return GvrControllerInput.ClickButtonUp; } }

        public override bool ClickButton { get { return GvrControllerInput.ClickButton; } }
        public override bool ClickButtonDown { get { return GvrControllerInput.ClickButtonDown; } }
        public override bool ClickButtonUp { get { return GvrControllerInput.ClickButtonUp; } }
#endif
    }

}