﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 카메라에 포함된 레티클로 선택이 가능한 오브젝트를 바라봤을경우에 페이크 클릭이벤트를 날려 실제로 클릭한것과 같이 보여지게한다.
/// </summary>
public class ReticleAutoSelector : MonoBehaviour
{
    UnityEngine.Object before;

	void Update ()
    {
        var pointer = GetComponent<GvrBasePointer>();
        if(before != pointer.CurrentRaycastResult.gameObject)
        {
            before = pointer.CurrentRaycastResult.gameObject;
            StopAllCoroutines();
            if (before)
            {
                StartCoroutine(testc());
            }
        }
	}
    IEnumerator testc()
    {
        yield return new WaitForSeconds(1);
        VRInput.GooglePlatform.ReticleClickButtonDown = true;
        VRInput.GooglePlatform.ReticleClickButton = true;
        yield return null;

        VRInput.GooglePlatform.ReticleClickButtonDown = false;
        yield return null;

        VRInput.GooglePlatform.ReticleClickButtonUp = true;
        yield return null;

        VRInput.GooglePlatform.ReticleClickButton = false;
        VRInput.GooglePlatform.ReticleClickButtonDown = false;
        VRInput.GooglePlatform.ReticleClickButtonUp = false;
    }
}
