﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using UnityEngine.Events;

/// <summary>
/// 컨트롤러 터치면을 스와이프한 상태에 대하여 Up,Down,Left,Right로 결과를 알려준다.
/// </summary>
public class ControllerSwipeEvent : SMonoBehaviour
{
    float startTime;
    Vector2 befPos;
    Vector2 recordPos;

    public UnityEvent onUp, onDown, onLeft, onRight;

    public float reduce = 0.9f;
    public float judgeMagnitude = 0.3f;

    private void Update()
    {
        if (VRInput.TouchDown)
        {
            recordPos = Vector2.zero;
            befPos = VRInput.TouchDir;
            startTime = Time.time;
        }
        else if (VRInput.IsTouching)
        {
            recordPos += VRInput.TouchDir - befPos;
            befPos = VRInput.TouchDir;
        }
        else if (VRInput.TouchUp)
        {
            if (recordPos.magnitude > judgeMagnitude)
            {
                //좌우 이동일 경우
                if (Mathf.Abs(recordPos.x) > Mathf.Abs(recordPos.y))
                {
                    if (recordPos.x < 0)
                    {
                        onLeft.Invoke();
                    }
                    else
                    {
                        onRight.Invoke();
                    }
                }
                //상하 이동일 경우
                else
                {
                    if (recordPos.y < 0)
                    {
                        onUp.Invoke();
                    }
                    else
                    {
                        onDown.Invoke();
                    }
                }
            }
        }

        recordPos *= reduce;
    }
}
