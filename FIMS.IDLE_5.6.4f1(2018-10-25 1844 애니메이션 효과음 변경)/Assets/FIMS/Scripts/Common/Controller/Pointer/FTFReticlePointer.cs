﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using UnityEngine.EventSystems;
using System.Collections;

namespace FTF
{
    /// <summary>
    /// 머리에 카메라 붙어있는 레티클을 제어하는 포인터다.
    /// GvrReticlePointer를 상속하고있으며 VRInput과 상호작용한다.
    /// 레티클의 모양 및 접촉하고 있는 상태에 대해서 판단하고 클릭의 역할을 한다.
    /// </summary>
    public class FTFReticlePointer : GvrReticlePointer
    {
        public bool autoSelect = true;

        Renderer rend;
        GvrBasePointer pointer;

        bool click;
        bool clickDown;
        bool clickUp;
        GameObject before;
        STweenState state;

        public override bool TriggerDown { get { return clickDown || VRInput.ClickButton; } }
        public override bool Triggering { get { return click || VRInput.ClickButton; } }
        public override bool TriggerUp { get { return clickUp || VRInput.ClickButtonUp; } }

        protected override void Start()
        {
            base.Start();
            pointer = GetComponent<GvrBasePointer>();
            rend = GetComponent<Renderer>();
        }

        protected override void Update()
        {
            base.Update();
            if (autoSelect)
            {
                if (before != pointer.CurrentRaycastResult.gameObject)
                {
                    before = pointer.CurrentRaycastResult.gameObject;

                    // 초기화
                    state.TryStop();
                    state = null;
                    StopAllCoroutines();
                    rend.material.SetFloat("_Clip", 0);

                    if (Check())
                    {
                        StartCoroutine(Clicker());
                    }
                }
            }
        }

        bool Check()
        {
            if (pointer.CurrentRaycastResult.gameObject)
            {
                if(pointer.CurrentRaycastResult.module is GvrPointerGraphicRaycaster)
                {
                    return true;
                }
                else if(pointer.CurrentRaycastResult.gameObject.GetComponent<IEventSystemHandler>() != null)
                {
                    return true;
                }
            }
            return false;
        }

        IEnumerator Clicker()
        {
            // 바라보는 시간(2018-07-13 오후 5:45에 수정함)
            //yield return (state = STween.value(0f, 1f, 1f, v =>
            yield return (state = STween.value(0f, 1f, 0.5f, v =>
            {
                rend.material.SetFloat("_Clip", v);
            }).SetOnComplete(()=> state = null)).Wait();

            clickDown = true;
            click = true;
            yield return null;

            clickDown = false;
            yield return null;

            clickUp = true;
            yield return null;

            click = false;
            clickUp = false;
        }

        [ContextMenu("TEST")]
        public void Create()
        {
            CreateReticleVertices();
        }

        protected override void CreateReticleVertices()
        {
            base.CreateReticleVertices();

            var mesh = GetComponent<MeshFilter>().mesh;

            Vector2[] uv = new Vector2[mesh.vertexCount];
            float uvseg = mesh.vertexCount / 2;
            float uvd = 1 / uvseg;
            for (int i = 0; i < uvseg; i++)
            {
                uv[i * 2 + 0] = new Vector2(i * uvd, 0);
                uv[i * 2 + 1] = new Vector2(i * uvd, 1);
            }

            mesh.uv = uv;
        }
    }
}