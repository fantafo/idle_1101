﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using UnityEngine.EventSystems;

namespace FTF
{
    /// <summary>
    /// 컨트롤러의 움직임을 나타낸다. 실제로 들어나는 컨트롤러가 1개라는 가정하에 만들어진 컴포넌트이며
    /// 데이드림 컨트롤러, 오큘러스컨트롤러 왼쪽, 오른쪽에 총3개가 설정된다.
    /// FTFPointerBridge.Instance.position, rotation으로 컨트롤러의 월드 위치, 회전값을 알 수 있다.
    /// </summary>
    public class FTFPointerBridge : SMonoBehaviour
    {
        public static FTFPointerBridge Instance { get; private set; }

        private void OnEnable()
        {
            if(Instance != null)
            {
                SLog.WarnFormat("FTFPointerBridge", "{0}이 이미 할당 돼 있는상태에서 {1}이 할당됐습니다.", Instance.name, name);
            }

            Instance = this;
        }

        private void OnDisable()
        {
            if (Instance == this)
                Instance = null;
        }

        public Vector3 position;
        public Quaternion rotation;

        private void Update()
        {
            if (transform.hasChanged)
            {
                position = MainPlayerController.main.cameraRig.transform.InverseTransformPoint(transform.position);
                rotation = MainPlayerController.main.cameraRig.transform.rotation.Inverse() * transform.rotation;
                transform.hasChanged = false;
            }
        }
    }
}