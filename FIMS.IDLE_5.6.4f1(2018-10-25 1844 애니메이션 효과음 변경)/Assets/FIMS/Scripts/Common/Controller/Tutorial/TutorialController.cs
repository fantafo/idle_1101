﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;

namespace FTF.Tutorial
{
    public enum ButtonState
    {
        None,
        Idle,
        Back,
        System,
        Touchpad,
        TouchPoint
    }

    public class TutorialController : MonoBehaviour
    {
        public AbsDummyController controller;
        public DummyDaydreamController daydreamController;
        public DummyGearController gearController;
        [Space]
        public ButtonState button;
        public bool blinkButtonOnStartup;

        private void Start()
        {
            if (VRInput.isDaydream)
                controller = daydreamController;
            else
                controller = gearController;

            controller.gameObject.SetActive(true);
            ResetController();

            if (blinkButtonOnStartup)
                BlinkCurrent(0, 1, 1, SLoopType.Clamp);
        }

        //================================================================================================
        //
        //                                       Action Methods
        //
        //================================================================================================

        /// <summary>
        /// 컨트롤러를 보여준다.
        /// </summary>
        public void SetActiveController(bool active)
        {
            controller.gameObject.SetActive(active);
        }

        /// <summary>
        /// 터치 포인트를 보여준다.
        /// </summary>
        /// <param name="active"></param>
        public void SetActiveTouchPoint(bool active)
        {
            controller.TouchPointAlpha = active ? 1 : 0;
        }

        /// <summary>
        /// 초기화한다
        /// </summary>
        public void ResetController()
        {
            controller.BackButtonPress = 0;
            controller.SystemButtonPress = 0;
            controller.TouchPointAlpha = 0;
            controller.TouchpadAlpha = 0;
        }

        /// <summary>
        /// 버튼을 선택한다.
        /// </summary>
        public void SetButton(ButtonState state)
        {
            SetButtonState(0);
            this.button = state;
        }

        /// <summary>
        /// 터치 포인터의 위치를 지정한다.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetTouchpoint(float x, float y)
        {
            controller.TouchPointX = x;
            controller.TouchPointY = y;
        }

        /// <summary>
        /// 버튼을 클릭할 수 있도록 보여준다.
        /// </summary>
        private void SetButtonState(float active)
        {
            switch (button)
            {
                case ButtonState.Back:
                    controller.BackButtonPress = active;
                    break;
                case ButtonState.System:
                    controller.SystemButtonPress = active;
                    break;
                case ButtonState.Touchpad:
                    controller.TouchpadAlpha = active;
                    break;
                case ButtonState.TouchPoint:
                    controller.TouchPointAlpha = Mathf.Clamp01(1 - ((Mathf.Abs(active - 0.5f) - 0.4f) * 10));
                    break;
            }
            controller.Validate();
        }


        //================================================================================================
        //
        //                                       Action Methods
        //
        //================================================================================================

        /// <summary>
        /// 현재 선택 돼 있는 버튼을 깜빡이게한다.
        /// </summary>
        public STweenState BlinkCurrent(float begin, float end, float duration, SLoopType loop)
        {
            return STween.value(begin, end, duration, (v) =>
            {
                SetButtonState(v);
            }).Set(loopType: SLoopType.PingPong, loop: 0);
        }

        /// <summary>
        /// 터치포인터를 세로로 올리거나 내리게한다.
        /// </summary>
        public STweenState MoveTouchPointY(float begin, float end, float duration, SLoopType loop, SEasingType ease)
        {
            SetButtonState(0);
            button = ButtonState.TouchPoint;
            SetButtonState(0);
            controller.TouchPointX = 0;

            return STween.value(0f, 1f, duration, (v) =>
            {
                controller.TouchPointY = (end - begin) * v + begin;
                SetButtonState(v);
            }).Set(loop: 0, loopType: loop, ease: ease);
        }
    }
}