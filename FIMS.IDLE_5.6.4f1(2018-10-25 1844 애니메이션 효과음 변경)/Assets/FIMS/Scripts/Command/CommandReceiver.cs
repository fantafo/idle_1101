﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FTF
{
    /// <summary>
    /// CommandListener에서 배포할 대상클래스이다.
    /// 로우패킷인 BinaryReader를 전달받아 처리할 수 있게된다.
    /// 
    /// CommandReceiver를 상속받은 클래스는 [OnRecieve(opcode)] Attribute를 사용할 수 있게된다.
    /// OnReceive 메서드는 등록된 opcode가 호출될때 BinaryReader를 전달받게 되어 처리할 수 있게된다.
    /// 
    /// opcode는 1~255까지 사용가능하며 0번은 AutoCommandReceiver에 예약돼 있다.
    /// </summary>
    public class CommandReceiver : SMonoBehaviour
    {
        //================================================================================================
        //
        //                                   Build Listener
        //
        //================================================================================================

        class Listener
        {
            public byte opcode;
            public Action<BinaryReader> listener;
        }
        List<Listener> addedListener;
        protected virtual void Awake()
        {
            addedListener = new List<Listener>();
            Type type = GetType();
            var methods = type.GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            for(int i=0; i<methods.Length; i++)
            {
                object[] attrs = methods[i].GetCustomAttributes(typeof(OnReceiveAttribute), false);
                if(!attrs.IsEmpty())
                {
                    byte opcode = ((OnReceiveAttribute)attrs[0]).opcode;
                    var act = (Action<BinaryReader>)Delegate.CreateDelegate(typeof(Action<BinaryReader>), this, methods[i]);
                    addedListener.Add(new Listener
                    {
                        opcode = opcode,
                        listener = act
                    });
                }
            }
            addedListener.TrimExcess();
        }

        //================================================================================================
        //
        //                                   Unity LifeCycle
        //
        //================================================================================================

        protected virtual void OnEnable()
        {
            for (int i = 0; i < addedListener.Count; i++)
            {
                CommandListener.AddListener(addedListener[i].opcode, addedListener[i].listener);
            }
        }

        protected virtual void OnDisable()
        {
            for (int i = 0; i < addedListener.Count; i++)
            {
                CommandListener.RemoveListener(addedListener[i].opcode, addedListener[i].listener);
            }
        }


        //================================================================================================
        //
        //                                      Helper Attribute
        //
        //================================================================================================

        [AttributeUsage(AttributeTargets.Method)]
        public class OnReceiveAttribute : Attribute
        {
            public byte opcode { get; private set; }

            public OnReceiveAttribute(byte opcode)
            {
                this.opcode = (byte)opcode;
            }
        }
    }
}
