﻿using FTF.CommandAuto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// CommandReceiver가 사용하기 어려운 경우 Unity의 SendMessage와 비슷한 용도로 사용할 수 있게 제공하는 클래스다.
    /// [OnCommandListener]를 사용한 메서드의 경우에만 SendMessage같이 사용할 수 있게된다.
    /// 또한 OnCommandListener로 지정된 메서드의 변수는 primity 타입 혹은 Vector, Quaternion, Rect, Bounds만 사용할 수 있다.
    /// </summary>
    public class AutoCommandReceiver : SMonoBehaviour
    {
        public CommandAuto.ClassWrapper delegator;

        private void Awake()
        {
            CommandAuto.CommandHelper.PutClass(GetType(), out delegator);
        }

        private void OnEnable()
        {
            CommandAuto.CommandHelper.PutClass(GetType(), out delegator);
            delegator.targets.Add(this);
        }

        private void OnDisable()
        {
            if (delegator != null)
                delegator.targets.Remove(this);
        }

        public void Broadcast(string methodName, params object[] param)
        {
            CommandHelper.Broadcast(GetType(), methodName, param);
        }
    }

    /// <summary>
    /// 메서드에 선언할 경우 해당 메서드는 커맨드를 받을 수 있게됩니다.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class OnCommandListenerAttribute : Attribute { }
}
namespace FTF.CommandAuto
{

    public class ClassWrapper
    {
        public Type type;
        public List<AutoCommandReceiver> targets = new List<AutoCommandReceiver>();
        public Dictionary<string, MethodWrapper> listeners = new Dictionary<string, MethodWrapper>();
    }

    public class MethodWrapper
    {
        public MethodInfo method;
        public Type[] parameters;
    }

    /// <summary>
    /// AutoCommand를 작동하기위한 내부 변수들을 준비하고, 할당, 해제를 하기위한 클래스
    /// 이 클래스는 외부에 노출되지 않으며 내부에서만 작동한다.
    /// </summary>
    internal static class CommandHelper
    {
        static CommandHelper()
        {
            CommandListener.AddListener(0, OnReceive);
        }

        /// <summary>
        /// AutoCommandReceiver에서 사용할 데이터를 만들어낸다.
        /// 데이터 빌드는 한번만 이뤄지며, 이후에는 저장된 데이터를 가져와 사용한다.
        /// 해당 type에서 선언된 OnCommandListenerAttribute를 모두 수집하여 Dictionary에 메서드 이름으로 저장된다.
        /// </summary>
        /// <param name="type">빌드할 클래스</param>
        /// <param name="delegator">인용할 수 있는 ClassWrapper 받을 주소</param>
        public static void PutClass(Type type, out ClassWrapper delegator)
        {
            if (!listenerType.TryGetValue(type, out delegator))
            {
                if (listenersName.ContainsKey(type.Name))
                {
                    Debug.LogError("AutoCommandReceiver 중에 동일한 이름을 가진 대상이 있습니다. AutoCommandReceiver의 클래스이름은 유일해야합니다. - " + type.Name);
                    return;
                }

                delegator = new ClassWrapper { type = type };
                listenersName.Add(type.Name, delegator);
                listenerType.Add(type, delegator);

                var methods = type.GetMethods(System.Reflection.BindingFlags.Public | BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                for (int i = 0; i < methods.Length; i++)
                {
                    var method = methods[i];
                    object[] attrs = method.GetCustomAttributes(typeof(OnCommandListenerAttribute), false);
                    if (!attrs.IsEmpty())
                    {
                        ParameterInfo[] param = method.GetParameters();
                        Type[] paramTypes = new Type[param.Length];
                        for (int j = 0; j < param.Length; j++)
                        {
                            paramTypes[j] = param[j].ParameterType;
#if UNITY_EDITOR
                            if (!typeConverter.ContainsKey(paramTypes[j]))
                            {
                                Debug.LogErrorFormat("'{0}.{1}'에서 사용할 수 없는 변수 타입을 사용했습니다. (PrimaryType과 Vector2,Vector3,Quaternion,Color만 사용할 수 있습니다.)");
                                if (UnityEditor.EditorApplication.isPlaying)
                                    UnityEditor.EditorApplication.isPlaying = false;
                                goto CONTINUE;
                            }
#endif
                        }

                        delegator.listeners.Add(method.Name, new MethodWrapper
                        {
                            method = method,
                            parameters = paramTypes
                        });
                    }
                    CONTINUE:
                    { }
                }
            }
        }

        /// <summary>
        /// Broadcast된 데이터를 서버를 통해 클라이언트에 전송되고,
        /// 전송된 자료가 PacketHandler->CommandListener->OnReceive에 제출된다.
        /// 제출된 자료를 이곳에서 가공하여 각각의 ClassWrapper들을 검색한다.
        /// 검색된 대상의 Delegate를 Invoke하여 메서드가 호출되게된다.
        /// </summary>
        private static void OnReceive(BinaryReader r)
        {
            r.ReadC(); // read Opcode
            string className = r.ReadS();
            string methodName = r.ReadS();

            ClassWrapper cw;
            if (listenersName.TryGetValue(className, out cw))
            {
                MethodWrapper method;
                if (cw.listeners.TryGetValue(methodName, out method))
                {
                    object[] invokeParams = new object[method.parameters.Length];
                    for (int i = 0; i < method.parameters.Length; i++)
                    {
                        invokeParams[i] = typeConverter[method.parameters[i]].read(r);
                    }

                    for (int i = 0; i < cw.targets.Count; i++)
                    {
                        method.method.Invoke(cw.targets[i], invokeParams);
                    }
                }
                else
                {
                    Debug.LogError("대상의 클래스가 등록되지 않았습니다. 버전이 맞는지 검사 해 주십시오.");
                }
            }
        }

        public static void Broadcast(Type type, string methodName, params object[] param)
        {
            ClassWrapper cw;
            if (listenerType.TryGetValue(type, out cw))
            {
                MethodWrapper mw;
                if (cw.listeners.TryGetValue(methodName, out mw))
                {
                    Command cmd = Command.Take();
                    cmd.WriteC(0);
                    cmd.WriteS(type.Name);
                    cmd.WriteS(methodName);

                    for (int i = 0; i < param.Length; i++)
                    {
                        try
                        {
                            typeConverter[mw.parameters[i]].write(cmd.GetWriter(), param[i]);
                        }
                        catch
                        {
                            Debug.LogErrorFormat("{0}.{1} 에서 {2}({4})를 {3}로 변환할 수 없습니다", type.Name, methodName, param[i], param[i].GetType().Name, mw.parameters[i].Name);
                            return;
                        }
                    }

                    cmd.BroadcastAll(true);
                }
                else
                {
                    Debug.LogErrorFormat("AutoCommandReceiver 중 {0}.{1} 메서드는 존재하지 않습니다.", type.Name, methodName);
                }
            }
            else
            {
                Debug.LogErrorFormat("AutoCommandReceiver 중 {0} 타입은 존재하지 않습니다.", type.Name);
            }
        }


        //================================================================================================
        //
        //                                          Parser
        //
        //================================================================================================

        /// <summary>
        /// 각 타입을 읽고 쓰기 위한 대리자
        /// </summary>
        class TypeConverter
        {
            public Func<BinaryReader, object> read;
            public Action<BinaryWriter, object> write;

            public TypeConverter(Func<BinaryReader, object> read, Action<BinaryWriter, object> write)
            {
                this.read = read;
                this.write = write;
            }
        }

        public static Dictionary<string, ClassWrapper> listenersName = new Dictionary<string, ClassWrapper>();
        public static Dictionary<Type, ClassWrapper> listenerType = new Dictionary<Type, ClassWrapper>();
        static Dictionary<Type, TypeConverter> typeConverter = new Dictionary<Type, TypeConverter>
        {
            { typeof(int),        new TypeConverter(r => r.ReadInt32()     , (r,v)=> r.Write(Convert.ToInt32(  v)) ) },
            { typeof(bool),       new TypeConverter(r => r.ReadBoolean()   , (r,v)=> r.Write(Convert.ToBoolean(v)) ) },
            { typeof(string),     new TypeConverter(r => r.ReadString()    , (r,v)=> r.Write(Convert.ToString( v)) ) },
            { typeof(float),      new TypeConverter(r => r.ReadSingle()    , (r,v)=> r.Write(Convert.ToSingle( v)) ) },
            { typeof(Vector3),    new TypeConverter(r => r.ReadVector3()   , (r,v)=> r.WriteV3((Vector3)       v) ) },
            { typeof(Vector2),    new TypeConverter(r => r.ReadVector2()   , (r,v)=> r.WriteV2((Vector2)       v) ) },
            { typeof(Quaternion), new TypeConverter(r => r.ReadQuaternion(), (r,v)=> r.WriteQ((Quaternion)     v) ) },
            { typeof(Color),      new TypeConverter(r => r.ReadColorRGBA() , (r,v)=> r.WriteRGBA((Color)       v) ) },
            { typeof(Rect),       new TypeConverter(r => r.ReadRect()      , (r,v)=> r.WriteRect((Rect)        v) ) },
            { typeof(Bounds),     new TypeConverter(r => r.ReadBounds()    , (r,v)=> r.WriteBounds((Bounds)    v) ) },
            { typeof(byte),       new TypeConverter(r => r.ReadByte()      , (r,v)=> r.Write(Convert.ToByte(   v)) ) },
            { typeof(short),      new TypeConverter(r => r.ReadInt16()     , (r,v)=> r.Write(Convert.ToInt16(  v)) ) },
            { typeof(long),       new TypeConverter(r => r.ReadInt64()     , (r,v)=> r.Write(Convert.ToInt64(  v)) ) },
            { typeof(sbyte),      new TypeConverter(r => r.ReadSByte()     , (r,v)=> r.Write(Convert.ToSByte(  v)) ) },
            { typeof(uint),       new TypeConverter(r => r.ReadUInt32()    , (r,v)=> r.Write(Convert.ToUInt32( v)) ) },
            { typeof(ushort),     new TypeConverter(r => r.ReadUInt16()    , (r,v)=> r.Write(Convert.ToUInt16( v)) ) },
            { typeof(ulong),      new TypeConverter(r => r.ReadUInt64()    , (r,v)=> r.Write(Convert.ToUInt64( v)) ) },
            { typeof(double),     new TypeConverter(r => r.ReadDouble()    , (r,v)=> r.Write(Convert.ToDouble( v)) ) },
        };
    }
}
