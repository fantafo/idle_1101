﻿using FTF.Packet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace FTF
{
    public interface IReceivePacket
    {
        void OnReceivePacket(ServerOpcode code, BinaryReader reader);
    }

    /// <summary>
    /// 패킷을 처리하기위한 기본단위이다. 패킷을 받아들여 저장할 수 있는 Queue와
    /// 그것을 꺼내어 Handling 메서드를 호출해주고 남은 패킷 찌꺼기는 BytePool에 반환한다.
    /// PacketHandler가 Mono에서 돌아가는 이유는 유니티의 변수들을 좀더 적극적으로 활용할 수 있게 함이다.
    /// </summary>
    public abstract class AbstractPacketHandler : SMonoBehaviour
    {
        public class MessageException : Exception
        {
            public MessageException(string msg) : base(msg) { }
        }

        public static Action<Action> OnQuit;

        #region Base Handling
        ////////////////////////////////////////////////////////
        //                                                    //
        //                  Member Variables                  //
        //                                                    //
        ////////////////////////////////////////////////////////

        // 패킷을 저장하기위한 컨테이너
        protected Queue<byte[]> remainPackets = new Queue<byte[]>();

        // 처리를 위한 변수
        protected byte[] buffer;
        protected MemoryStream memoryStream;

        protected BinaryReader reader;
        protected ServerOpcode code { get; set; }
        protected ILog log = SLog.GetLogger(typeof(NetworkerPacketHandler));


        ////////////////////////////////////////////////////////
        //                                                    //
        //                  Network                           //
        //                                                    //
        ////////////////////////////////////////////////////////

        /// <summary>
        /// Networker에서 데이터를 전달받는다.
        /// </summary>
        public void Push(byte[] data)
        {
            lock (remainPackets)
            {
                remainPackets.Enqueue(data);
            }
        }

        ////////////////////////////////////////////////////////
        //                                                    //
        //                  LifeCycles                        //
        //                                                    //
        ////////////////////////////////////////////////////////

        protected virtual void Awake()
        {
            buffer = new byte[ushort.MaxValue];
            memoryStream = new MemoryStream(buffer, true);
            reader = new BinaryReader(memoryStream, Encoding.UTF8);
        }

        protected virtual void OnDestroy()
        {
            try
            { reader.Close(); }
            catch { }
            try
            { memoryStream.Close(); }
            catch { }
            buffer = null;
        }

        protected virtual void Update()
        {
            byte[] data;
            int procSize = remainPackets.Count;
            for (int i = 0; i < procSize; i++)
            {
                // 데이터 읽기 및 초기화
                lock (remainPackets)
                { data = remainPackets.Dequeue(); }
                Array.Copy(data, buffer, data.Length);
                memoryStream.Position = 0;

                try
                {
                    code = reader.ReadServerCode();
                    Handling();
                }
                catch (Exception e)
                {
                    log.ErrorFormat("Exception Opcode {0}({1}) / Size: {2}byte\n{3}", code.ToString(), (short)code, data.Length, HexString.ToString(data, data.Length));
                    log.Exception(e);
                }
                finally
                {
                    BytePool.Release(data);
                }
            }
        }
        #endregion

        protected abstract void Handling();
    }
}