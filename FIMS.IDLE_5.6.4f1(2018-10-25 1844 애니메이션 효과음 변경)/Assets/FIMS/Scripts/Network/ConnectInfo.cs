﻿using System;
using System.IO;
using UnityEngine;

public class ConnectInfo : SMonoBehaviour
{
    ////////////////////////////////////////////////////////
    //                                                    //
    //                 Static Variables                   //
    //                                                    //
    ////////////////////////////////////////////////////////

    static ConnectInfo main;
    static Properties prop = new Properties();
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    static void Check() { if (main == null) throw new NullReferenceException("ConnectInfo가 로딩되지 않았습니다."); }


    // user
    public static long UserSN { get { return prop.GetLong("user.sn", main.sn); } }
    public static string UserName { get { return prop.GetString("user.name", main.userName); } }

    // client
    public static string ConnectIP { get { return prop.GetString("client.connect.ip", main.connectIP); } }
    public static int ConnectPort { get { return prop.GetInt("client.connect.port", main.connectPort); } }

    // room
    public static int RoomID { get { return prop.GetInt("room.id", main.roomID); } }
    public static string RoomName { get { return prop.GetString("room.name", main.roomName); } }
    public static string RoomPassword { get { return prop.GetString("room.password", main.roomPassword); } }
    public static int MapID { get { return prop.GetInt("room.map", main.roomMapID); } }
    public static int UserCount { get { return prop.GetInt("room.user.count", main.roomUserCount); } }



    ////////////////////////////////////////////////////////
    //                                                    //
    //                 Member Variables                   //
    //                                                    //
    ////////////////////////////////////////////////////////

    [Header("User")]
    public long sn = 0;
    public string userName = null;

    [Header("Client")]
    public string connectIP = "127.0.0.1";
    public int connectPort = 40000;

    [Header("Room")]
    public int roomID = -1;
    public int roomMapID = 1;
    public string roomName = "-";
    public string roomPassword = "-";
    public int roomUserCount = 1;



    ////////////////////////////////////////////////////////
    //                                                    //
    //                 Life Cycles                        //
    //                                                    //
    ////////////////////////////////////////////////////////

    private void Awake()
    {
        if (main)
        {
            gameObject.Destroy();
            return;
        }

#if FTF_OBSERVER
        // TODO: 하드코딩변경
        try{
            connectPort = int.Parse(File.ReadAllText("D:/Publish/CommonServer/port"));
        }catch{}
#endif

        main = this;
        prop.Clear();

        userName = FimsCommonData.GetFlag("Name", null) ?? userName;

        if (File.Exists(FimsCommonData.CONNECT_PATH))
        {
            prop.Load(FimsCommonData.CONNECT_PATH);
        }
    }

    private void OnDestroy()
    {
        if (main == this)
        {
            main = null;
            prop.Clear();
        }
    }
}
