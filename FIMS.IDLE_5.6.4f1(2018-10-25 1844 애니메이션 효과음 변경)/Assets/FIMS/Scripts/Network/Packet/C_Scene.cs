﻿using System;

namespace FTF.Packet
{
    /// <summary>
    /// 방에서 씬을 실행 한 뒤 관리를 위한 패킷 모음
    /// </summary>
    public class C_Scene
    {
        public enum UpdateType
        {
            Absolute,   // 절대치
            Relative    // 현재를 기준으로 변경
        }

        public static byte[] InitializeCompleted(int instanceID)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Scene_Initialized);
            w.WriteD(instanceID);
            return w.ToBytes();
        }
        public static byte[] UpdateScore(UpdateType type, int score)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Scene_UpdateScore);
            w.WriteD(0);
            w.WriteC((byte)type);
            w.WriteD(score);
            return w.ToBytes();
        }
        public static byte[] UpdateScore(int instanceID, UpdateType type, int score)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Scene_UpdateScore);
            w.WriteD(instanceID);
            w.WriteC((byte)type);
            w.WriteD(score);
            return w.ToBytes();
        }
    }
}