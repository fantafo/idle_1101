﻿using System.IO;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 서버에서 전달된 데이터가 직접적으로 수정되는 곳이다.
    /// 순수한 데이터로만 존재하며 서버의 요청에따라 언제든 수정될 수 있다.
    /// 수정될때는 OnReceivePacket을 통해서 수정된다.
    /// </summary>
    public class PlayerInstance : IReceivePacket
    {
        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Static Variables                   //
        //                                                    //
        ////////////////////////////////////////////////////////

        public static PlayerInstance main; // 실제 캐릭터
        public static PlayerInstance lookMain; // 화면에 보여지는 캐릭터 (옵저버 사용시, 바라볼 유저를 이곳에 넣는다)
        public static bool IsObserver { get { return main != lookMain; } }

        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Member Variables                   //
        //                                                    //
        ////////////////////////////////////////////////////////

        // 연결 정보
        public int instanceId;
        public bool IsConnected;

        // 개인정보
        public string userName;
        public int level;
        public int exp;

        // 캐릭터정보
        public short headType;
        public short bodyType;

        // 방정보
        public int roomIndex;
        public long roomID;
        public bool isReady;

        // 게임정보
        public short currentHP;
        public short maxHP;
        public int score;

        // 보이스쳇
        public string voiceIP;
        public int voicePort;

        public bool isDirty;

        //--
        public PlayerInfo info;

        public bool IsRoomMaster { get { return roomIndex == 0; } }



        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Network                            //
        //                                                    //
        ////////////////////////////////////////////////////////

        public void OnReceivePacket(ServerOpcode code, BinaryReader reader)
        {
            switch (code)
            {
                case ServerOpcode.C_UserInfo:
                    {
                        userName = reader.ReadS();
                        level = reader.ReadH();
                        exp = reader.ReadD();

                        currentHP = reader.ReadH();
                        maxHP = reader.ReadH();
                        score = reader.ReadD();

                        isReady = reader.ReadB();
                        IsConnected = reader.ReadB();

                        roomID = reader.ReadL();
                        roomIndex = reader.ReadC();

                        headType = reader.ReadH();
                        bodyType = reader.ReadH();

                        voiceIP = reader.ReadS();
                        voicePort = reader.ReadH();

                        if (this != PlayerInstance.lookMain && info != null && info.voice != null)
                        {
                            info.voice.SourceIP = voiceIP;
                            info.voice.SourcePort = voicePort;
                        }
                    }
                    break;
            }
        }
    }
}