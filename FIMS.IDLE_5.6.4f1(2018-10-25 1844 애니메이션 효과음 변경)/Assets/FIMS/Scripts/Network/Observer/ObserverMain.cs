﻿using FTF.Packet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace FTF
{
    /// <summary>
    /// 옵저버의 MainPlayerController 의 역할을 하며, 옵저가 활성화 돼 있는 동안,
    /// 해당 MainPlayerController 외의 컨트롤러는 감춰져 있어야한다.
    /// 이 컨트롤러의 ObserverSchedule 코루틴으로 인해 옵저버의 행동이 재현되며,
    /// ObserverPacketHandler를 통해 원하는 데이터를 취득할 수 있다.
    /// </summary>
    public class ObserverMain : FTFPlayerController
    {
        public ObserverScheduler scheduler = new ObserverScheduler();
        public CanvasGroup messageGroup;
        public int changeNextPlayer = 0;

        protected override void Start()
        {
            base.Start();
            StartCoroutine(ObserverSchedule());

            // 이름 초기화
            var text = messageGroup.GetComponentInChildren<Canvas>().GetComponentInChildren<Text>();
            text.text = Application.productName;
        }

        protected override void UpdateModelTransform()
        {
            if(info.model != null)
            {
                cameraRig.transform.position = info.model.headContainer.position;
                cameraRig.transform.rotation = info.model.headContainer.rotation;
            }
        }

        IEnumerator ObserverSchedule()
        {
            ScreenFader.main.FadeOut(0);
            while (Networker.State < NetworkState.Validating)
                yield return null;

            Networker.main.send(C_Common.Version(Networker.main.clientIdentifier, Networker.main.clientPacketVersion, false));
            yield return scheduler.Wait(ServerOpcode.C_ServerVersion);

            Networker.Send(C_Common.LoginObserver());
            yield return scheduler.Wait(ServerOpcode.C_LoginResult);

            STweenState animFader = null;
            while (true)
            {
                // 방목록을 받으면 새로운 방으로 들어가게된다.
                Networker.Send(C_Channel.RequestList());
                yield return scheduler.Wait(ServerOpcode.Room_List);

                // 로딩완료를 기다린다
                bool isNewRoom = scheduler.RequestNext;
                if (isNewRoom)
                {
                    if (Networker.State == NetworkState.Playing)
                    {
                        animFader.TryStop();
                        animFader = STween.alpha(messageGroup, 1, 1).SetOnComplete(() => animFader = null);
                        Networker.Send(C_Room.Exit());
                        ScreenFader.main.FadeOut();

                        yield return scheduler.Wait(ServerOpcode.Scene_End, ServerOpcode.Room_Exit);
                        yield return Waits.Wait(2);

                    }

                    Networker.Send(C_Channel.Join((long)scheduler.ResultValue, ConnectInfo.RoomPassword));
                    yield return scheduler.Wait(ServerOpcode.Room_Info);

                    // 시청할 첫번째 대상 지정
                    for (int i = 0; i < RoomInfo.main.players.Length; i++)
                    {
                        if (RoomInfo.main.players[i] != null && RoomInfo.main.players[i].instance != null)
                        {
                            PlayerInstance.lookMain = RoomInfo.main.players[i].instance;
                        }
                    }

                    // 게임이 시작될 때까지 대기한다
                    yield return scheduler.Wait(ServerOpcode.Scene_Start);

                    animFader.TryStop();
                    animFader = STween.alpha(messageGroup, 0, 1).SetOnComplete(() => animFader = null);
                }

                // 각 플레이어들을 돌면서 보여준다.
                for (int i = 0; i < RoomInfo.main.players.Length; i++)
                {
                    if (RoomInfo.main.players[i] != null && RoomInfo.main.players[i].instance != null)
                    {
                        Debug.Log("VIEW "+i);
                        PlayerInstance.lookMain = RoomInfo.main.players[i].instance;
                        yield return Waits.Wait(changeNextPlayer);
                    }
                }
                
                // 새로운 방에 들어온 방이 아니라면 패킷 속도를 위해 1초정도 기다린다.
                if (isNewRoom == false)
                    yield return Waits.Wait(1);
            }
        }

    }

    public class ObserverScheduler
    {
        ServerOpcode[] waitCode;
        public bool IsComplete;
        public bool RequestNext;
        public object ResultValue { get; set; }

        public IEnumerator Wait(params ServerOpcode[] code)
        {
            waitCode = code;
            IsComplete = false;
            RequestNext = false;
            while (!IsComplete)
            {
                yield return null;
            }
        }
        public void Next(ServerOpcode code, object value = null)
        {
            ResultValue = value;
            foreach (var cd in waitCode)
            {
                if (cd == code)
                {
                    goto NEXT;
                }
            }
            return;

            NEXT:
            {
                IsComplete = true;
                RequestNext = true;
            }
        }
        public void Break(ServerOpcode code)
        {
            foreach (var cd in waitCode)
            {
                if (cd == code)
                {
                    goto NEXT;
                }
            }
            return;

            NEXT:
            {
                IsComplete = true;
                RequestNext = false;
            }
        }
    }
}
