﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;

namespace FTF
{
    /// <summary>
    /// 사용할 시스템의 내부 버전을 정의합니다.
    /// </summary>
    public static class Version
    {
        public const int CommonPacketVersion = 2;
    }
}