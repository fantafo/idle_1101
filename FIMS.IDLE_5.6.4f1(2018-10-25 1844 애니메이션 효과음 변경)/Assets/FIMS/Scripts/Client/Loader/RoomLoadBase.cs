﻿using FTF.Packet;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FTF
{
    /// <summary>
    /// RoomInfo.onLoadStart에 씬로드 시작의 이벤트를 등록하는 기반을 한다.
    /// 이 클래스를 상속하고 RoomInfo와 같은 객체에 등록해놓으면 자동으로 사용된다.
    /// </summary>
    public abstract class RoomLoadBase : LoadBase
    {
        protected RoomInfo room;

        protected virtual void Awake()
        {
            room = GetComponent<RoomInfo>();
            room.onLoadStart.AddListener(LoadStart);
        }
    }
}