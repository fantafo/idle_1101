﻿using FTF.Packet;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FTF
{
    /// <summary>
    /// 씬을 ASync로 읽어올때 사용하는 abstract클래스다.
    /// 일반적으로 Room의 씬을 읽어올때 사용한다.
    /// </summary>
    public abstract class LoadBase : SMonoBehaviour
    {
        bool fadeComplete;
        bool isLoading;

        protected abstract bool LoadValidate();
        protected abstract AsyncOperation LoadAsync();
        protected virtual void LoadCompelte() { }

        protected virtual void LoadStart()
        {
            if (isLoading)
                return;
            isLoading = true;

            if (LoadValidate())
            {
                if (MainPlayerController.main != null)
                {
                    fadeComplete = false;
                    ScreenFader.main.FadeOut(() => fadeComplete = true);
                }
                else
                {
                    fadeComplete = true;
                }
                StartCoroutine(onLoadStart());
            }
        }
        IEnumerator onLoadStart()
        {
            var op = LoadAsync();
            
            op.allowSceneActivation = false;
            while (!fadeComplete)
                yield return null;

            while (op.progress < .9f)
                yield return null;

            Networker.Send(C_Room.LoadComplete());

            while (Networker.State != NetworkState.Playing)
                yield return null;

            op.allowSceneActivation = true;
            LoadCompelte();
            isLoading = false;
        }
    }
}