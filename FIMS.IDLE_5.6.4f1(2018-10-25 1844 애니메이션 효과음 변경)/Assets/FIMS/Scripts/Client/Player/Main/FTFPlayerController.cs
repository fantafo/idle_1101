﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using System.Collections;
using FTF.Packet;
using FTF.VoIP;

namespace FTF
{
    /// <summary>
    /// PlayerInstance.main만 바라보고 모든 일을 처리한다.
    /// </summary>
    public class FTFPlayerController : MainPlayerController
    {
        public PlayerInfo info { get { return PlayerInstance.lookMain != null ? PlayerInstance.lookMain.info : null; } }

        protected virtual void Start()
        {
            StartCoroutine(SendingVoipInformation());
        }
        IEnumerator SendingVoipInformation()
        {
            string beforeIPAddress = null;
            int beforePort = -1;

            while (true)
            {
                yield return Waits.Wait(1);

                if(beforeIPAddress != VoiceServer.ServerIPAddress || beforePort != VoiceServer.ServerPort)
                {
                    Networker.Send(C_Common.VoIPSetup(
                        beforeIPAddress = VoiceServer.ServerIPAddress,
                        beforePort = VoiceServer.ServerPort));
                }
            }
        }

        protected virtual void Update()
        {
            if (info != null)
            {
                transform.position = info.transform.position;
                transform.rotation = info.transform.rotation;

                UpdateModelTransform();
            }
            else
            {
                transform.position = Vector3.zero;
                transform.rotation = Quaternion.identity;
            }
        }
        protected virtual void UpdateModelTransform()
        {
            if (info.model != null)
            {
                info.model.headPosition = VRInput.headPosition;
                info.model.headRotation = VRInput.headOrientation;

                info.model.handPosition = VRInput.controllerPosition;
                info.model.handRotation = VRInput.controllerOrientation;
            }
        }
    }
}