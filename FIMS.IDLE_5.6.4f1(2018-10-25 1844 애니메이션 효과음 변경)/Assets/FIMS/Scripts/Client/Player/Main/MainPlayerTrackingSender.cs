﻿using FTF.Packet;
using System.Collections;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// PlayerInstance.main만 바라보고 모든 일을 처리한다.
    /// </summary>
    public class MainPlayerTrackingSender : SMonoBehaviour
    {
        public float delay = 0.05f;

        private void OnEnable()
        {
            StartCoroutine(SendData());
        }

        private IEnumerator SendData()
        {
            WaitForSeconds wait = new WaitForSeconds(delay);
            while (true)
            {
                if (Networker.State >= NetworkState.Room)
                {
                    Networker.Send(C_Common.TrackingPosition(), true);
                }

                yield return wait;
            }
        }
    }
}