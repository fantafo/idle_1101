﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;

namespace FTF
{
    public class PlayerHead : SMonoBehaviour
    {
        public enum State
        {
            None = -1,
            Default,
            Attack,
            Die,
        }

        public Transform topPosition;
        public GameObject[] _states;

        State _state = State.None;
        public State state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    _state = value;
                    int st = (int)value;
                    for (int i = 0; i < _states.Length; i++)
                    {
                        _states[i].SetActive(i == st);
                    }
                }
            }
        }

    }
}