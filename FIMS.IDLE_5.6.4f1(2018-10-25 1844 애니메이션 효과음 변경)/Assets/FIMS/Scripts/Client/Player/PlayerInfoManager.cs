﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// PlayerInfo를 관리하는 매니저로서 씬에 꼭 포함 돼 있어야한다.
    /// 플레이어를 맵에 보여주기 위해 새로 생성하거나, 삭제, 관리 등을 한다.
    /// </summary>
    public class PlayerInfoManager : SMonoBehaviour
    {
        public static PlayerInfoManager main;

        //================================================================================================
        //
        //                                       Member Variables                                  
        //
        //================================================================================================

        public GameObject playerPrefab;
        List<PlayerInfo> activePlayers = new List<PlayerInfo>();
        Stack<PlayerInfo> pools = new Stack<PlayerInfo>();

        //================================================================================================
        //
        //                                      Unity LifeCycle
        //
        //================================================================================================
        private void Awake()
        {
            if (main)
            {
                gameObject.Destroy();
            }

            main = this;
        }
        private void OnDestroy()
        {
            if (main == this)
            {
                main = null;
            }
        }

        /// <summary>
        /// 새로운 플레이어를 발견했을고, 맵에 보여주기 위해 할당할때 사용한다.
        /// 모든 PlayerInfo의 객체는 이 메서드를 통해서 제작돼야한다.
        /// 또한 생성되는 PlayerInfo는 playerPrefab으로 지정돼 있는 대상이며,
        /// 없을경우 기본타입으로 생성된다.
        /// </summary>
        /// <returns></returns>
        public PlayerInfo TakePlayer()
        {
            if (pools.Count > 0)
            {
                return pools.Pop();
            }
            else
            {
                GameObject go;
                if (playerPrefab != null)
                {
                    go = Instantiate(playerPrefab, transform);
                }
                else
                {
                    go = new GameObject("player", typeof(PlayerInfo));
                    go.transform.SetParent(transform, false);
                }
                go.transform.ResetLocal();

                var player = go.GetComponent<PlayerInfo>();
                if (player == null)
                    throw new InvalidCastException(playerPrefab.name + " 프리펩에서 PlayerInfo 컴포넌트를 찾을 수 없습니다.");

                return player;
            }
        }
        public void ReleasePlayer(PlayerInfo info)
        {
            activePlayers.Remove(info);
            pools.Push(info);
        }

        public PlayerInfo FindPlayer(PlayerInstance inst)
        {
            for (int i = 0; i < activePlayers.Count; i++)
            {
                if (activePlayers[i].instance == inst)
                {
                    return activePlayers[i];
                }
            }
            return null;
        }
    }
}