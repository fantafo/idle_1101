﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 유저의 위치를 지정하기위한 좌표로 사용된다.
    /// </summary>
    public class UserLocation: SMonoBehaviour
    {
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.DrawCube(Vector3.zero, Vector3.one * 0.1f);
            Gizmos.DrawCube(new Vector3(0, 0, .2f), new Vector3(.02f, .02f, .4f));

            var style = new GUIStyle(UnityEditor.EditorStyles.toolbar);
            style.alignment = TextAnchor.MiddleCenter;
            UnityEditor.Handles.Label(transform.position + new Vector3(0,0.25f,0), name, style);
            
        }
#endif
    }
}