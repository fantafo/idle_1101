﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 유저 좌표를 갖고있는 매니저
    /// </summary>
    public class UserLocationManager : SMonoBehaviour
    {
        public static UserLocationManager main;

        public List<UserLocation> locates;

        void Start()
        {
            main = this;
        }

        [Button]
        public void CollectLocates()
        {
            GetComponentsInChildren(locates);
        }
    }
}