﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace FTF
{
    /// <summary>
    /// Recenter()가 호출되면 컨트롤러의 Recenter 버튼을 누른것과 같은 이벤트가 실행된다.
    /// </summary>
    public class RecenterAction : SMonoBehaviour
    {
        public Text counter;
        public int duration;
        bool processing;

        public void Recenter()
        {
            if (processing) return;
            processing = true;

            gameObject.SetActive(true);
            StartCoroutine(Recentering());
        }

        private IEnumerator Recentering()
        {
            for(int i=0; i<duration; i++)
            {
                counter.text = (duration - i).ToString();
                yield return Waits.Wait(1);
            }
            UnityEngine.VR.InputTracking.Recenter();
            processing = false;
            gameObject.SetActive(false);
        }
    }
}