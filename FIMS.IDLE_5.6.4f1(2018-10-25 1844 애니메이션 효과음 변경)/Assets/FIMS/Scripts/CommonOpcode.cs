﻿using System.IO;

public static class BasePacketExtension
{
    public const short OPCODE_SIZE = 1;

    public static void WriteO(this BinaryWriter w, ClientOpcode value) { w.Write((byte)value); }
    public static void WriteO(this BinaryWriter w, ServerOpcode value) { w.Write((byte)value); }

    public static ClientOpcode ReadClientCode(this BinaryReader r) { return (ClientOpcode)r.ReadByte(); }
    public static ServerOpcode ReadServerCode(this BinaryReader r) { return (ServerOpcode)r.ReadByte(); }
}

public enum ClientOpcode : byte
{
    None = 0,
    C_ClientVersion = 0x01,     // [가장최초패킷] 서버에게 클라이언트 버전을 보내고 서버버전을 기다립니다. 
    C_Ping,                     // 서버에서 전송해온 핑패킷을 최대한 빠르게 반환해야합니다. (전송속도를 측정하기위함)
    C_Login,                    // 아이디 비밀번호가 속해있는 패킷을 서버에 보냅니다.
    C_Quit,                     // 게임을 종료할때 보냅니다.
    C_TrackPosition,            // 자신의 트래킹정보를 서버에 보냅니다.
    C_Request_UserInfo,             // 캐릭터정보를 요청합니다.
    C_VoIPSetup,                // 보이스쳇을 설정합니다.
    C_Command,                  // 커스텀 커맨드를 전체 유저에게 브로드케스트합니다.

    Channel_RequestRoomList,    // 전체 방정보를 요청합니다.
    Channel_CreateRoom,         // 방을 생성합니다.
    Channel_JoinRoom,           // 방에 접속합니다.

    Room_Exit,                  // 방에서 나갑니다.
    Room_Ban,                   // 특정 유저를 강퇴합니다.
    Room_ChangeLevel,           // 레벨을 바꿉니다.
    Room_Ready,                 // 준비를 합니다.
    Room_LoadComplete,          // 로딩이 완료됐다고 보내줍니다.

    Scene_Initialized,          // 유저가 다른 유저에 대한 세팅이 끝났을때 서버에 완료됐다고 보내준다.
    Scene_UpdateScore,          // 유저의 점수 정보를 갱신해준다.

    Voice_Initialized,          // 음성데이터를 초기화합니다
    Voice_Data,                 // 녹음한 음성데이터 패킷
}
public enum ServerOpcode : byte
{
    None = 0,
    C_ServerVersion = 0x01,     // 서버 버전을 보내서 서로의 버전을 확인한다.
    C_Ping,                     // 더미 패킷을 보내서 서버와의 연결유지를 확인하고, 서로의 전송속도도 확인한다.
    C_LoginResult,              // 로그인 결과를 보냅니다
    C_UserInfo,                // 유저정보를 보냅니다
    C_Disconnect,               // 유저를 강제종료할 때 보내줍니다.
    C_SystemMessage,            // 유저에게 표시해야할 공통UI 메세지를 보냅니다.
    C_Command,                  // 유저가 보낸 커스텀 커맨드를 브로드캐스트합니다.

    Room_List,                  // 방목록을 보냅니다.
    Room_Info,                  // 방의 정보를 보냅니다.
    Room_Exit,                  // 방이 파괴될 때 보내집니다.
    Room_LoadStart,             // 모두가 레디가 완료되면 보냅니다.
    Room_Ban,                   // 특정유저가 강퇴당합니다.

    Scene_Start,                // 씬이 시작됐다고 알립니다.
    Scene_End,                  // 씬이 종료됐다고 알립니다.

    Scene_InitializeTo,         // 중간에 참여한 유저를 위해 기존에 참여하고 있는 유저가 설정을 다시 해주는 과정

    Voice_Initialized,          // 음성데이터를 초기화합니다
    Voice_Data,                 // 녹음한 음성데이터 패킷
}