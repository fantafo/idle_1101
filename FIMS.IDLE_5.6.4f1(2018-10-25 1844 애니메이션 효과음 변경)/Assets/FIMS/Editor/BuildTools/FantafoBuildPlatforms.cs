﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using egl = UnityEditor.EditorGUILayout;
using gl = UnityEngine.GUILayout;
using System;
using UnityEngine.VR;
using UnityEditorInternal.VR;
using System.Linq;
using System.IO;

public class FantafoBuildPlatforms : EditorWindow
{

    [MenuItem("File/Build FTF #&b")]
    public static void ShowBuildPlatforms()
    {
        GetWindowWithRect<FantafoBuildPlatforms>(new Rect(100, 200, 700, 250), true, "FTF Build Panel", true);
    }

    const string SAVE_FILE_PATH = "ftf_build.txt";

    public FantafoBuildPlatforms()
    {
        if (File.Exists(SAVE_FILE_PATH))
        {
            try
            {
                string[] paths = File.ReadAllLines(SAVE_FILE_PATH);
                path = paths[0];
                buildGearVR = bool.Parse(paths[1]);
                buildDaydream = bool.Parse(paths[2]);
                buildCardboard = bool.Parse(paths[3]);
                buildGoogleVR = bool.Parse(paths[4]);
                buildObserver = bool.Parse(paths[5]);
                observerPath = paths[6];
                buildDebugging = bool.Parse(paths[7]);
            }
            catch { }
        }
        Save();
    }

    void Save()
    {
        File.WriteAllLines(SAVE_FILE_PATH,
            new string[]
            {
                path,
                buildGearVR.ToString(),
                buildDaydream.ToString(),
                buildCardboard.ToString(),
                buildGoogleVR.ToString(),
                buildObserver.ToString(),
                observerPath,
                buildDebugging.ToString()
            });
    }

    string path = "runtime.{0}.apk";
    string observerPath = "Observer.exe";
    bool buildGearVR = true;
    bool buildDaydream = true;
    bool buildCardboard = true;
    bool buildObserver = true;
    bool buildGoogleVR = true;
    bool buildDebugging = false;
    bool isDirty = false;

    public BuildOptions GetBuildOptions
    {
        get
        {
            BuildOptions result = BuildOptions.None;
            if (buildDebugging)
            {
                result |= BuildOptions.Development;
                result |= BuildOptions.SymlinkLibraries;
                result |= BuildOptions.ConnectWithProfiler;
                result |= BuildOptions.AllowDebugging;
            }
            return result;
        }
    }

    private void OnGUI()
    {
        bool isBuild = false;
        GUI.changed = false;
        if (!path.Contains("{0}"))
        {
            isDirty = true;
            ShowNotification(new GUIContent("파일명에 {0}이 입력돼야 합니다. 해당 부분에 gear 혹은 google로 기록됩니다."));
        }
        else if (isDirty)
        {
            isDirty = false;
            RemoveNotification();
        }

        EditorGUIUtility.labelWidth = 100;
        gl.Space(20);
        gl.BeginHorizontal();
        gl.Space(10);
        gl.BeginVertical();
        {
            // apk 파일 위치
            gl.BeginHorizontal();
            try
            {
                path = egl.TextField("Apk", path);
                if (gl.Button("..", gl.Width(25)))
                {
                    ChooseApkFile();
                }
            }
            catch { }
            gl.EndHorizontal();
            // Observer 파일 위치
            gl.BeginHorizontal();
            try
            {
                observerPath = egl.TextField("Observer", observerPath);
                if (gl.Button("..", gl.Width(25)))
                {
                    ChooseObserverFile();
                }
            }
            catch { }
            gl.EndHorizontal();

            gl.Space(10);
            gl.Label("Build Target");
            gl.BeginVertical("Box");
            try
            {
                buildGearVR = egl.Toggle("Gear VR", buildGearVR);
                buildDaydream = egl.Toggle("Daydream", buildDaydream);
                buildCardboard = egl.Toggle("Cardboard", buildCardboard);
                buildObserver = egl.Toggle("Observer", buildObserver);
                gl.Space(10);
                buildDebugging = egl.Toggle("Allow Debugging", buildDebugging);
            }
            catch { }
            gl.EndVertical();

            if (GUI.changed)
                try
                {
                    Save();
                }
                catch { }

            gl.FlexibleSpace();
            gl.BeginHorizontal();
            {
                gl.FlexibleSpace();
                if (gl.Button("Build", gl.Width(100)))
                {
                    isBuild = true;
                }
            }
            gl.EndHorizontal();
        }
        gl.EndVertical();
        gl.Space(10);
        gl.EndHorizontal();
        gl.Space(30);

        if (isBuild)
        {
            try
            {
                Build();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
    }

    BuildStack before = new BuildStack();
    public struct BuildStack
    {
        public string[] supportDevices;
        public string define;
        internal AndroidSdkVersions minSdkVersion;
    }

    private bool ChooseApkFile()
    {
        string path =
            EditorUtility.SaveFilePanel("Select Apk Location",
            Path.GetDirectoryName(this.path),
            Path.GetFileName(this.path).Replace("{0}", "").Replace(".apk", "").Trim(new char[] { '.' }),
            "apk");

        if (path.Exists())
        {
            path = path.Replace('\\', '/');
            if (!path.Contains("{0}") && path.Contains(".apk"))
                path = path.Replace(".apk", ".{0}.apk");

            string curr = Path.GetFullPath(".").Replace('\\', '/');
            if (path.StartsWith(curr))
                path = path.Substring(curr.Length);

            path = path.Trim(new char[] { ' ', '/' });
            this.path = path;
            Save();
            Repaint();
            return true;
        }
        return false;
    }
    private bool ChooseObserverFile()
    {
        string path =
            EditorUtility.SaveFilePanel("Select Observer File",
                Path.GetDirectoryName(observerPath),
                Path.GetFileName(observerPath),
                "exe");

        if(path.Exists())
        {
            this.observerPath = path;
            Save();
            Repaint();
            return true;
        }
        return false;
    }

    private void Build()
    {
        BuildDefaultSetting();
        try
        {
            if (buildObserver)
                if (BuildObserver(observerPath) == false) return;

            if (buildGearVR)
                if (BuildGearVRPlatform(string.Format(path, "oculus")) == false) return;

            if (buildDaydream)
                if (BuildDaydreamPlatform(string.Format(path, "daydream")) == false) return;

            if (buildCardboard)
                if (BuildCardboardPlatform(string.Format(path, "cardboard")) == false) return;
        }
        finally
        {
            Debug.Log("<color=#00FFFF>[ Revert ]</color>");
            Revert();
        }
    }

    ///////////////////////////////////////////////////////////////////
    /// 
    /// Default Setting
    /// 
    ///////////////////////////////////////////////////////////////////
    private void BuildDefaultSetting()
    {
        // 빌드 전 세팅 저장
        before = new BuildStack();
        before.supportDevices = VREditor.GetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android);
        before.define = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
        before.minSdkVersion = PlayerSettings.Android.minSdkVersion;

        // 빌드전 현재 AndroidManifest를 복제해둔다.
        if (!Directory.Exists("Assets/FIMS/Plugins/Android/.old"))
            Directory.CreateDirectory("Assets/FIMS/Plugins/Android/.old");
        RemoveAndroidManifest(".old");
        foreach (var manifest in Directory.GetFiles("Assets/FIMS/Plugins/Android", "AndroidManifest*", SearchOption.TopDirectoryOnly))
            File.Move(manifest, "Assets/FIMS/Plugins/Android/.old/" + Path.GetFileName(manifest));

        // 텍스쳐 압축방식을 ASTC로 설정한다.
        if (EditorUserBuildSettings.androidBuildSubtarget != MobileTextureSubtarget.ASTC)
            EditorUserBuildSettings.androidBuildSubtarget = MobileTextureSubtarget.ASTC;

        // 빌드 타겟을 Android로 변경한다.
        if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android)
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);

        // Android으 VR 옵션을 켠다
        if (!VREditor.GetVREnabledOnTargetGroup(BuildTargetGroup.Android))
            VREditor.SetVREnabledOnTargetGroup(BuildTargetGroup.Android, true);

        // PlayerSettings 변수 적용
        PlayerSettings.stereoRenderingPath = StereoRenderingPath.MultiPass;
        PlayerSettings.gpuSkinning = true;
        PlayerSettings.mobileMTRendering = true;
        PlayerSettings.MTRendering = true;
        PlayerSettings.graphicsJobs = true;
        PlayerSettings.Android.targetDevice = AndroidTargetDevice.ARMv7;
        PlayerSettings.Android.forceInternetPermission = true;
        PlayerSettings.Android.forceSDCardPermission = true;
        PlayerSettings.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
        PlayerSettings.SetStackTraceLogType(LogType.Warning, StackTraceLogType.None);
    }

    private void Revert()
    {
        VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, before.supportDevices);
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, before.define);
        PlayerSettings.Android.minSdkVersion = before.minSdkVersion;

        RemoveAndroidManifest();
        CopyAndroidManifest(".old");
        RemoveAndroidManifest(".old");
        Directory.Delete("Assets/FIMS/Plugins/Android/.old", true);

        SetPlugin("OVRPlugin", true);
        SetPlugin("libaudioplugingvrunity", true);
        SetPlugin("gvr-kehyboard", true);
        SetPlugin("gvr-keyboardsupport-release", true);
        SetPlugin("gvr-permissionsupport-release", true);
    }


    ///////////////////////////////////////////////////////////////////
    /// 
    /// Build Oculus - Gear VR
    /// 
    ///////////////////////////////////////////////////////////////////
    private bool BuildGearVRPlatform(string path)
    {
        Debug.Log("<color=#00FFFF>[ Build <b>Gear VR</b> Platform ]</color>");
        VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, new string[] { "Oculus" });

        string define = (before.define + ";FTF_GEARVR").Replace("FTF_OBSERVER", "").Replace(";;", ";");
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, define);
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel21;

        SetPlugin("OVRPlugin", true);
        SetPlugin("libaudioplugingvrunity", false);
        SetPlugin("gvr-kehyboard", false);
        SetPlugin("gvr-keyboardsupport-release", false);
        SetPlugin("gvr-permissionsupport-release", false);
        AssetDatabase.SaveAssets();

        RemoveAndroidManifest();
        CopyAndroidManifest(".oculus");
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);

        var result = BuildPipeline.BuildPlayer(new BuildPlayerOptions
        {
            scenes = EditorBuildSettingsScene.GetActiveSceneList(EditorBuildSettings.scenes),
            locationPathName = path,
            target = BuildTarget.Android,
            targetGroup = BuildTargetGroup.Android,
            options = BuildOptions.None
        });
        if (result.Exists())
        {
            Debug.LogError("<b>BuildError: </b>" + result);
            return false;
        }
        return true;
    }


    ///////////////////////////////////////////////////////////////////
    /// 
    /// Build Daydream
    /// 
    ///////////////////////////////////////////////////////////////////
    private bool BuildDaydreamPlatform(string path)
    {
        Debug.Log("<color=#00FFFF>[ Build <b>Daydream</b> Platform ]</color>");
        VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, new string[] { "daydream" });

        string define = (before.define + ";FTF_DAYDREAM;FTF_GOOGLE").Replace("FTF_OBSERVER", "").Replace(";;", ";");
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, define);
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel24;

        SetPlugin("OVRPlugin", false);
        SetPlugin("libaudioplugingvrunity", true);
        SetPlugin("gvr-kehyboard", true);
        SetPlugin("gvr-keyboardsupport-release", true);
        SetPlugin("gvr-permissionsupport-release", true);
        AssetDatabase.SaveAssets();

        RemoveAndroidManifest();
        CopyAndroidManifest(".google");
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);

        var result = BuildPipeline.BuildPlayer(new BuildPlayerOptions
        {
            scenes = EditorBuildSettingsScene.GetActiveSceneList(EditorBuildSettings.scenes),
            locationPathName = path,
            target = BuildTarget.Android,
            targetGroup = BuildTargetGroup.Android,
            options = BuildOptions.None
        });
        if (result.Exists())
        {
            Debug.LogError("<b>BuildError: </b>" + result);
            return false;
        }
        return true;
    }


    ///////////////////////////////////////////////////////////////////
    /// 
    /// Build Google - Cardboard
    /// 
    ///////////////////////////////////////////////////////////////////
    private bool BuildCardboardPlatform(string path)
    {
        VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, new string[] { "cardboard" });

        Debug.Log("<color=#00FFFF>[ Build <b>Cardboard</b> Platform ]</color>");
        string define = (before.define + ";FTF_CARDBOARD;FTF_GOOGLE").Replace("FTF_OBSERVER", "").Replace(";;", ";");
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, define);
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel19;
        PlayerSettings.stereoRenderingPath = StereoRenderingPath.MultiPass;
        PlayerSettings.gpuSkinning = true;
        PlayerSettings.mobileMTRendering = true;
        PlayerSettings.MTRendering = true;
        PlayerSettings.graphicsJobs = true;

        SetPlugin("OVRPlugin", false);
        SetPlugin("libaudioplugingvrunity", true);
        SetPlugin("gvr-kehyboard", true);
        SetPlugin("gvr-keyboardsupport-release", true);
        SetPlugin("gvr-permissionsupport-release", true);
        AssetDatabase.SaveAssets();

        RemoveAndroidManifest();
        CopyAndroidManifest(".google");
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);

        var result = BuildPipeline.BuildPlayer(new BuildPlayerOptions
        {
            scenes = EditorBuildSettingsScene.GetActiveSceneList(EditorBuildSettings.scenes),
            locationPathName = path,
            target = BuildTarget.Android,
            targetGroup = BuildTargetGroup.Android,
            options = BuildOptions.None
        });
        if (result.Exists())
        {
            Debug.LogError("<b>BuildError: </b>" + result);
            return false;
        }
        return true;
    }


    ///////////////////////////////////////////////////////////////////
    /// 
    /// Build Observer
    /// 
    ///////////////////////////////////////////////////////////////////
    private bool BuildObserver(string path)
    {
        Debug.Log("<color=#00FFFF>[ Build <b>Observer</b> Platform ]</color>");
        var result = BuildPipeline.BuildPlayer(new BuildPlayerOptions
        {
            scenes = EditorBuildSettingsScene.GetActiveSceneList(EditorBuildSettings.scenes),
            locationPathName = path,
            target = BuildTarget.StandaloneWindows,
            targetGroup = BuildTargetGroup.Standalone,
            options = BuildOptions.None
        });
        if (result.Exists())
        {
            Debug.LogError("<b>BuildError: </b>" + result);
            return false;
        }
        return true;
    }


    ///////////////////////////////////////////////////////////////////
    /// 
    /// Helper
    /// 
    ///////////////////////////////////////////////////////////////////
    void RemoveAndroidManifest(string name = "")
    {
        foreach (var manifest in Directory.GetFiles("Assets/FIMS/Plugins/Android/" + name, "AndroidManifest*", SearchOption.TopDirectoryOnly))
        {
            File.Delete(manifest);
        }
    }

    void CopyAndroidManifest(string name)
    {
        foreach (var manifest in Directory.GetFiles("Assets/FIMS/Plugins/Android/" + name, "AndroidManifest*", SearchOption.TopDirectoryOnly))
        {
            File.Copy(manifest, "Assets/FIMS/Plugins/Android/" + Path.GetFileName(manifest));
        }
    }

    void SetPlugin(string name, bool active, string platform = "Android")
    {
        var paths = (from assetPath in AssetDatabase.GetAllAssetPaths()
                     where assetPath.StartsWith("Assets/")
                     where assetPath.Contains("/"+platform+"/")
                     where assetPath.Contains(name)
                     select assetPath);
        foreach (var path in paths)
        {
            PluginImporter importer = AssetImporter.GetAtPath(path) as PluginImporter;
            if (importer.ShouldIncludeInBuild())
            {
                importer.SetCompatibleWithPlatform(BuildTarget.Android, active);
                importer.SaveAndReimport();
                //Debug.Log("PluginEnable : " + path + "=" + active, importer);
            }
        }
    }
}
