﻿using System;
using System.Reflection;
using UnityEngine;

[AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
public class ButtonAttribute : PropertyAttribute
{
    public string labelName;
    public string methodName;
    public bool playOnly;
    public int space;

    public bool shouldDraw { get { return !playOnly || Application.isPlaying; } }

    public ButtonAttribute(string methodName = null, string labelName = null, bool playOnly = false)
    {
        this.methodName = methodName;
        this.labelName = string.IsNullOrEmpty(labelName) ? methodName : labelName;
        this.playOnly = playOnly;
    }

    public MethodInfo GetMethod(object o)
    {
        return o.GetType().GetMethod(methodName);
    }
}