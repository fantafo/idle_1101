﻿using System;
using System.IO;
using UnityEngine;

public static class BinaryStreamExtension
{
    //================================================================================================
    //
    //                                        Writeable
    //
    //================================================================================================
    public static void WriteRGB(this BinaryWriter self, Color32 value)
    {
        self.Write(value.r);
        self.Write(value.g);
        self.Write(value.b);
    }
    public static void WriteRGBA(this BinaryWriter self, Color32 value)
    {
        self.Write(value.r);
        self.Write(value.g);
        self.Write(value.b);
        self.Write(value.a);
    }
    public static void WriteV3(this BinaryWriter self, Vector3 value)
    {
        self.Write(value.x);
        self.Write(value.y);
        self.Write(value.z);
    }
    public static void WriteV2(this BinaryWriter self, Vector2 value)
    {
        self.Write(value.x);
        self.Write(value.y);
    }
    public static void WriteQ(this BinaryWriter self, Quaternion value)
    {
        self.Write(value.x);
        self.Write(value.y);
        self.Write(value.z);
        self.Write(value.w);
    }
    public static void WriteRect(this BinaryWriter self, Rect value)
    {
        self.Write(value.x);
        self.Write(value.y);
        self.Write(value.width);
        self.Write(value.height);
    }
    public static void WriteBounds(this BinaryWriter self, Bounds value)
    {
        self.WriteV3(value.center);
        self.WriteV3(value.extents);
    }

    public static void WriteB(this BinaryWriter self, bool value) { self.Write(value); }
    public static void WriteC(this BinaryWriter self, byte value) { self.Write(value); }
    public static void WriteC(this BinaryWriter self, int value) { self.Write((byte)value); }
    public static void WriteH(this BinaryWriter self, short value) { self.Write(value); }
    public static void WriteH(this BinaryWriter self, int value) { self.Write((short)value); }
    public static void WriteD(this BinaryWriter self, int value) { self.Write(value); }
    public static void WriteL(this BinaryWriter self, long value) { self.Write(value); }
    public static void WriteF(this BinaryWriter self, float value) { self.Write(value); }
    public static void WriteS(this BinaryWriter self, string value) { self.Write(value ?? string.Empty); }
    public static void WriteEC<T>(this BinaryWriter self, T v) where T : struct, IConvertible { self.WriteC(v.ToByte(null)); }
    public static void WriteEH<T>(this BinaryWriter self, T v) where T : struct, IConvertible { self.WriteH(v.ToInt16(null)); }


    //================================================================================================
    //
    //                                        Readable
    //
    //================================================================================================

    public static Color32 ReadColorRGB(this BinaryReader self)
    {
        return new Color32(self.ReadByte(), self.ReadByte(), self.ReadByte(), (byte)0xFF);
    }
    public static Color32 ReadColorRGBA(this BinaryReader self)
    {
        return new Color32(self.ReadByte(), self.ReadByte(), self.ReadByte(), self.ReadByte());
    }
    public static Vector2 ReadVector2(this BinaryReader self)
    {
        return new Vector2(self.ReadSingle(), self.ReadSingle());
    }
    public static Vector3 ReadVector3(this BinaryReader self)
    {
        return new Vector3(self.ReadSingle(), self.ReadSingle(), self.ReadSingle());
    }
    public static Quaternion ReadQuaternion(this BinaryReader self)
    {
        return new Quaternion(self.ReadSingle(), self.ReadSingle(), self.ReadSingle(), self.ReadSingle());
    }
    public static Rect ReadRect(this BinaryReader self)
    {
        return new Rect(self.ReadSingle(), self.ReadSingle(), self.ReadSingle(), self.ReadSingle());
    }
    public static Bounds ReadBounds(this BinaryReader self)
    {
        return new Bounds(self.ReadVector3(), self.ReadVector3());
    }

    public static bool ReadB(this BinaryReader self) { return self.ReadBoolean(); }
    public static byte ReadC(this BinaryReader self) { return self.ReadByte(); }
    public static short ReadH(this BinaryReader self) { return self.ReadInt16(); }
    public static int ReadD(this BinaryReader self) { return self.ReadInt32(); }
    public static long ReadL(this BinaryReader self) { return self.ReadInt64(); }
    public static float ReadF(this BinaryReader self) { return self.ReadSingle(); }
    public static string ReadS(this BinaryReader self) { return self.ReadString(); }
    public static T ReadEC<T>(this BinaryReader self) where T : struct, IConvertible { return (T)Enum.ToObject(typeof(T), self.ReadByte()); }
    public static T ReadEH<T>(this BinaryReader self) where T : struct, IConvertible { return (T)Enum.ToObject(typeof(T), self.ReadInt16()); }

}