﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

public class LookAtCameraAndScale : AbsTracker
{
    public Camera _target;
    public Vector3 _offset;
    public float _scale = 1;

    public override void OnReposition()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && _target == null)
            return;
#endif
        Transform target = (_target != null ? _target.transform : Camera.main.transform);
        Vector3 direction = target.position - transform.position;
        transform.rotation = Quaternion.LookRotation(direction) * Quaternion.Euler(_offset);
        transform.localScale = Vector3.one * (_scale * direction.magnitude);
    }
}
