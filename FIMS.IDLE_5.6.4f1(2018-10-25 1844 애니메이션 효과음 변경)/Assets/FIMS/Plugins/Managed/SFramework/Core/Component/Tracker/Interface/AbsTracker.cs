﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public abstract class AbsTracker : SMonoBehaviour, ITracker
{
    public Object _next;
    public bool _once;
    ITracker _nextTracker;

    protected virtual void Start()
    {
        if (_next is ITracker)
            _nextTracker = (ITracker)_next;
        else if (_next is GameObject)
            _nextTracker = ((GameObject)_next).GetComponent<ITracker>();
        if (_nextTracker is MonoBehaviour)
            ((MonoBehaviour)_nextTracker).enabled = false;
    }

    private void OnEnable()
    {
        if (_once)
            Reposition();
    }

    void LateUpdate()
    {
#if UNITY_EDITOR
        if (UnityEditor.EditorApplication.isCompiling)
            return;
#endif
        if (!_once)
            Reposition();
    }

    public void Reposition()
    {
        OnReposition();
        if (_nextTracker != null)
            _nextTracker.Reposition();
    }

    public abstract void OnReposition();
}
