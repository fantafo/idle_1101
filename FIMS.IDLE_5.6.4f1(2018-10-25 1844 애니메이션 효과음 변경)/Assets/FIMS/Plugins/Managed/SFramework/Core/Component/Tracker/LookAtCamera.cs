﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

[ExecuteInEditMode]
public class LookAtCamera : AbsTracker
{
    public Camera _target;
    public Vector3 _offset;

    public override void OnReposition()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && _target == null && Camera.main == null)
            return;
#endif
        Transform target = (_target != null ? _target.transform : Camera.main.transform);
        transform.rotation = Quaternion.LookRotation(target.position - transform.position) * Quaternion.Euler(_offset);
    }
}
