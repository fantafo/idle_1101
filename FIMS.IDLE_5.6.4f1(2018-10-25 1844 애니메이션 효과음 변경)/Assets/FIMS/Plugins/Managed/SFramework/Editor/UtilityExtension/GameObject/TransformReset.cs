﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class TransformReset : BaseExtensionEditor
    {


        [MenuItem("GameObject/Reset/Keep Child On World %&#t")]
        public static void ResetTransformChildWorldPosition()
        {
            if (init("Reset Transform & Child WorldPosition", 0))
            {
                Loop(t =>
                {
                    Transform[] childs = new Transform[t.childCount];
                    for (int i = 0; i < childs.Length; i++)
                    {
                        childs[i] = t.GetChild(0);
                        childs[i].SetParent(null);
                    }

                    t.localPosition = Vector3.zero;
                    t.localScale = Vector3.one;
                    t.localRotation = Quaternion.identity;

                    for (int i = 0; i < childs.Length; i++)
                    {
                        childs[i].SetParent(t);
                    }
                });
            }
        }

    }
}