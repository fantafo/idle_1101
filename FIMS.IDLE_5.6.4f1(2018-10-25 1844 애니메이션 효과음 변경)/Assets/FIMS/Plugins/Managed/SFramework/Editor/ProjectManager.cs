﻿using UnityEngine;
using UnityEngine.VR;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using System.IO;

/// <summary>
/// Author : 정상철
/// </summary>
namespace SFramework.Editors
{
    [InitializeOnLoad]
    public class ProjectManager
    {
        public static event Action update;
        public static event Action playModeChanged;
        public static List<Action> invoke = new List<Action>();

        static ProjectManager()
        {
            EditorApplication.update += Update;

            string path = "project_default_key.txt";
            if (File.Exists(path))
            {
                string[] lines = File.ReadAllLines(path);
                if (lines.Length > 2)
                {
                    PlayerSettings.Android.keystorePass = lines[0];
                    PlayerSettings.Android.keyaliasName = lines[1];
                    PlayerSettings.Android.keyaliasPass = lines[2];
                }
            }
            else
            {
                File.Create(path);
            }
        }

        static void Update()
        {
            if (EditorApplication.isCompiling)
            {
                update = null;
                playModeChanged = null;

                // 게임중 컴파일하면 네트워크에서 무한루프를 돌게되니 강제종료한다.
                if (EditorApplication.isPlaying)
                {
                    EditorApplication.isPlaying = false;
                }
            }
            if (update != null)
                update();

            if (invoke.Count > 0)
                lock (invoke)
                {
                    foreach (Action a in invoke)
                        a();
                    invoke.Clear();
                }
        }

        public static void Invoke(Action action)
        {
            lock (invoke)
            {
                invoke.Add(action);
            }
        }
    }
}