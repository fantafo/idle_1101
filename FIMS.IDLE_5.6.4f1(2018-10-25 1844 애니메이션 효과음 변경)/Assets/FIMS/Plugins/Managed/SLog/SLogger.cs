﻿#if UNITY_EDITOR
#define LOG_UNITY
#endif

#if LOG_ANDROID && UNITY_EDITOR
#undef LOG_ANDROID
#endif

using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using System.IO;
using System.Threading;

public class SLogger : ILog
{
#if UNITY_ANDROID && LOG_ANDROID
    public static AndroidJavaClass logClass;
    [RuntimeInitializeOnLoadMethod]
    public static void Initialize()
    {
        logClass = new AndroidJavaClass("android.util.Log");
    }
#endif



    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // MEMBER
    //
    /////////////////////////////////////////////////////////////////////////////////////////////

    public string tag = "SLog";
    public string Tag { get { return tag; } set { tag = value; } }

    public SLogger(string tag)
    {
        this.tag = tag;
    }
    public SLogger(object tag)
    {
        this.tag = tag.GetType().Name;
    }
    public SLogger(Type tag)
    {
        this.tag = tag.Name;
    }


    public void DebugFormat(string message, params object[] args) { Debug(tag, string.Format(message, args)); }
    public void Debug(string message) { Debug(tag, message); }
    public void Debug(string tag, string message)
    {
#if LOG_UNITY
#if LOG_ANDROID
        if(logClass != null)
            logClass.CallStatic<int>("d", "SLog", "["+ tag + "]"+ message);
        else
#endif
        UnityEngine.Debug.Log(tag + "/ " + message);
#endif
#if LOG_FILE
        writeFile('D', tag, message);
#endif
    }

    public void WarnFormat(string message, params object[] args) { Warn(tag, string.Format(message, args)); }
    public void Warn(string message) { Warn(tag, message); }
    public void Warn(string tag, string message)
    {
#if LOG_UNITY
#if LOG_ANDROID
        if(logClass != null)
            logClass.CallStatic<int>("w", "SLog", "["+ tag + "]"+ message);
        else
#endif
        UnityEngine.Debug.LogWarning(tag + "/ " + message);
#endif
#if LOG_FILE
        writeFile('W', tag, message);
#endif
    }

    public void ErrorFormat(string message, params object[] args) { Error(tag, string.Format(message, args)); }
    public void Error(string message) { Error(tag, message); }
    public void Error(string tag, string message)
    {
#if LOG_UNITY
#if LOG_ANDROID
        if(logClass != null)
            logClass.CallStatic<int>("e", "SLog", "["+ tag + "]"+ message);
        else
#endif
        UnityEngine.Debug.LogError(tag + "/ " + message);
#endif
#if LOG_FILE
        writeFile('E', tag, message);
#endif
    }

    public void Exception(Exception e) { Exception(tag, e); }
    public void Exception(string tag, Exception e)
    {
#if LOG_UNITY
#if LOG_ANDROID
        if(logClass != null)
            logClass.CallStatic<int>("e", "SLog", "["+ tag + "]"+ e.Message + "\r\n"+ e.StackTrace);
        else
#endif
        UnityEngine.Debug.LogException(e);
#endif
#if LOG_FILE
        writeFile('E', tag, e.Message + "\r\n" + e.StackTrace);
#endif
    }


#if LOG_FILE

#if !UNITY_EDITOR && UNITY_ANDROID
    const string PATH_ROOT = "/sdcard/Fantafo/log";
#else
    const string PATH_ROOT = "./log";
#endif
    static object lockObj = new object();
    static StreamWriter fw;

    public static void writeFile(char type, string tag, string message)
    {
        lock (lockObj)
        {
            if(fw == null)
            {
                string logFolder = string.Format("{0}/{1}", PATH_ROOT, Application.identifier);
                if (!Directory.Exists(logFolder))
                {
                    Directory.CreateDirectory(logFolder);
                }

                string logFile = string.Format("{0}/{1:yyyy-MM-dd HH;mm;ss}.txt", logFolder, DateTime.Now);
                fw = new StreamWriter(new FileStream(logFile, FileMode.CreateNew, FileAccess.Write, FileShare.Read), Encoding.UTF8);
            }

            fw.WriteLine(string.Format("{0:HH:mm:ss.fff}\t{1}\t[{2,-20}]\t{3,-20}\t{4}", DateTime.Now, type, Thread.CurrentThread.Name,  tag, message));
            fw.Flush();
        }
    }
#endif
}