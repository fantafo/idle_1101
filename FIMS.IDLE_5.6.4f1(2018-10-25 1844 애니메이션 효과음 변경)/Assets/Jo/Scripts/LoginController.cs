﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

using System.IO;

namespace RenderHeads.Media.AVProVideo
{
    public class LoginController : SMonoBehaviour
    {
        //[SerializeField]
        //private static string str_InputTxt = "";

        //private string str_Num = "";
        ILog log = SLog.GetLogger(typeof(LoginController));

        public GameObject _title;
        public GameObject _tutorialZero;
        public GameObject _zeroBtn;
        public GameObject _tutorialOne;
        public GameObject _oneBtn;
        public GameObject _tutorialTwo;
        public GameObject _twoBtn;
        public GameObject _tutorialThree;
        public GameObject _threeBtn;
        public GameObject _calibrationBGObj;
        public GameObject _loginBGObj;

        public GameObject _particleSystemObj;
        public GameObject _soundBtn1;
        public GameObject _soundBtn2;
        public GameObject _soundBtn3;
        public GameObject _soundBtn4;
        public GameObject _gazeButtonObj;
        public GameObject _gazeImageObj;
        public GameObject _toLoginButtonObj;

        public GameObject[] go_InputObjs;
        public Button[] _numButtons;
        public GameObject[] _numEffects;
        public Button _deleteButton;
        public Button _initializeButton;
        public Button _nextButton;
        public Text _msgText;

        public Button[] _allButtons;

        public GameObject _confirmBGObj;
        public Text _userIdText;
        public Text _userNameText;

        private Text txt;
        private string _strPath = "/storage/emulated/0/Fantafo/Flags/";

        private int _currentNumTextIdx = 0;

        private bool isNotOne = true;

        private Coroutine _co = null;
        private Coroutine _gazeImageBlinkCo = null;
        private Coroutine _afterToLoginButtonClickedCo = null;
        private Coroutine _checkOKFileCo = null;

        private Vector3 _myScale = new Vector3(0, 0, 0);        

        private void Start()
        {
            _myScale = _loginBGObj.transform.localScale;
            
            _soundBtn1.SetActive(true);
            _soundBtn3.SetActive(true);
            _soundBtn4.SetActive(true);
            _title.SetActive(true);

            _tutorialZero.SetActive(false);
            _zeroBtn.SetActive(false);
            _oneBtn.SetActive(false);
            _twoBtn.SetActive(false);
            _threeBtn.SetActive(false);
            _tutorialOne.SetActive(false);
            _tutorialTwo.SetActive(false);
            _tutorialThree.SetActive(false);
            _calibrationBGObj.SetActive(false);
            _toLoginButtonObj.SetActive(false);
            _loginBGObj.SetActive(false);
            _confirmBGObj.SetActive(false);
            _particleSystemObj.SetActive(false);
            
            Initialize();

            //if (null != _gazeImageBlinkCo)
            //{
            //    StopCoroutine(_gazeImageBlinkCo);
            //}
            //_gazeImageBlinkCo = StartCoroutine(this.GazeImageBlinkCoroutine());

            if (null != _checkOKFileCo)
            {
                StopCoroutine(_checkOKFileCo);
            }
            _checkOKFileCo = StartCoroutine(this.CheckOKFileCoroutine());
        }

        private void Update()
        {
            if(_title.GetComponentInChildren<MediaPlayer>().Control.IsFinished() && isNotOne)
            {
                TitleEnd();
                //_title.SetActive(false);
                //_tutorialOne.SetActive(true);

            }
        }

        // 응시 버튼 깜빡이는 연출 코루틴
        private IEnumerator GazeImageBlinkCoroutine()
        {
            WaitForSeconds delay = new WaitForSeconds(0.5f);

            //while (_gazeButtonObj.activeSelf == true)
            //{
            //    yield return delay;
            //    _gazeImageObj.SetActive(true);
            //    yield return delay;
            //    _gazeImageObj.SetActive(false);
            //}

            yield return new WaitForSeconds(2.0f);

            //_toLoginButtonObj.SetActive(true);
        }

        //튜토리얼 페이지 넘길때만 사용

        public void TitleEnd() { StartCoroutine(WaitingTitleEnd()); }
        private IEnumerator WaitingTitleEnd()
        {
            _title.SetActive(false);
            _soundBtn3.GetComponent<AudioSource>().Play();
            _tutorialOne.SetActive(true);            
            yield return new WaitForSeconds(2.0f);
            _oneBtn.SetActive(true);

        }

        public void OnClickNextOne() { StartCoroutine(WaitTimingOne()); }
        private IEnumerator WaitTimingOne()
        {
            _soundBtn1.GetComponent<AudioSource>().Play();
            _soundBtn3.GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.5f);
            _tutorialOne.SetActive(false);
            isNotOne = false;
            yield return new WaitForSeconds(1.5f);
            _soundBtn4.GetComponent<AudioSource>().Play();
            _tutorialTwo.SetActive(true);
            yield return new WaitForSeconds(2.0f);
            _twoBtn.SetActive(true);
        }

        public void OnClickNextTwo() { StartCoroutine(WaitTimingTwo()); }
        private IEnumerator WaitTimingTwo()
        {
            _soundBtn1.GetComponent<AudioSource>().Play();
            _soundBtn3.GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.5f);
            _tutorialTwo.SetActive(false);
            yield return new WaitForSeconds(1.5f);
            _soundBtn4.GetComponent<AudioSource>().Play();
            _tutorialThree.SetActive(true);
            yield return new WaitForSeconds(2.0f);
            _threeBtn.SetActive(true);
        }

        public void OnClickNextThree() { StartCoroutine(WaitTimingThree()); }
        private IEnumerator WaitTimingThree()
        {
            _soundBtn1.GetComponent<AudioSource>().Play();
            _soundBtn3.GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.5f);
            _tutorialThree.SetActive(false);
            yield return new WaitForSeconds(1.5f);
            _soundBtn4.GetComponent<AudioSource>().Play();
            _tutorialZero.SetActive(true);
            yield return new WaitForSeconds(5.0f);
            _zeroBtn.SetActive(true);
        }

        public void OnClickNextZero() { StartCoroutine(WaitTimingZero()); }
        private IEnumerator WaitTimingZero()
        {
            _soundBtn1.GetComponent<AudioSource>().Play();
            _soundBtn3.GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.5f);
            isNotOne = false;
            _tutorialZero.SetActive(false);
            yield return new WaitForSeconds(1.5f);
            _soundBtn4.GetComponent<AudioSource>().Play();
            _calibrationBGObj.SetActive(true);
        }
        ////사용 안함.
        //private IEnumerator NextPageOne()
        //{
        //    yield return new WaitForSeconds(3.0f);
        //    _tutorialZero.SetActive(false);
        //    yield return new WaitForSeconds(1.5f);
        //    _tutorialOne.SetActive(true);
        //}




        // 응시 버튼 눌렸을 때 처리
        public void OnClickGazeButton()
        {
            _gazeButtonObj.SetActive(false);
            _gazeImageObj.SetActive(false);
            _particleSystemObj.SetActive(true);
            _toLoginButtonObj.SetActive(true);
        }

        private IEnumerator WaitTimingCali()
        {

            yield return new WaitForSeconds(0.5f);
            _gazeButtonObj.SetActive(false);
            _gazeImageObj.SetActive(false);
            _particleSystemObj.SetActive(true);
            _toLoginButtonObj.SetActive(true);
        }

        // 로그인 화면으로 버튼 눌렀을 때 처리
        public void OnClickToLoginButton()
        {
            if (null != _afterToLoginButtonClickedCo)
            {
                StopCoroutine(_afterToLoginButtonClickedCo);
            }

            _afterToLoginButtonClickedCo = StartCoroutine(this.AfterToLoginButtonClickedCoroutine());

            _soundBtn1.GetComponent<AudioSource>().Play();
        }

        private IEnumerator AfterToLoginButtonClickedCoroutine()
        {
            _soundBtn3.GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.5f);
            _calibrationBGObj.SetActive(false);
            yield return new WaitForSeconds(2.0f);
            _soundBtn4.GetComponent<AudioSource>().Play();
            _loginBGObj.SetActive(true);
        }

        public void OnClickNumButton(int num)
        {
            //if (str_InputTxt.Length < go_Input.Length)
            //{
            //    str_InputTxt = str_InputTxt.Insert(str_InputTxt.Length, str_Num);
            //}
            go_InputObjs[_currentNumTextIdx].GetComponentInChildren<Text>().text = num.ToString();
            Debug.Log("로그인 숫자패드" + num.ToString());

            if (_currentNumTextIdx < go_InputObjs.Length)
            {
                ++_currentNumTextIdx;
            }
            _soundBtn1.GetComponent<AudioSource>().Play();
        }

        public void Delete()
        {
            if (_currentNumTextIdx > 0)
            {
                --_currentNumTextIdx;
            }

            go_InputObjs[_currentNumTextIdx].GetComponentInChildren<Text>().text = "";
            _soundBtn1.GetComponent<AudioSource>().Play();
        }

        public void Initialize()
        {
            Debug.Log(go_InputObjs.Length);
            for (int i = 0; i < go_InputObjs.Length; i++)
            {
                go_InputObjs[i].GetComponentInChildren<Text>().text = "";
            }
            _currentNumTextIdx = 0;
            _soundBtn1.GetComponent<AudioSource>().Play();
        }

        // 버튼 위에 조준점 올라갔을 때 호출되는 함수
        public void OnPointerEnterButton(int index)
        {
            if (_allButtons[index].interactable == false)
            {
                return;
            }
            //이펙트 적용
            _numEffects[index].gameObject.SetActive(true);
            // 버튼 확대        
            iTween.ScaleTo(_allButtons[index].gameObject, Vector3.one * 1.3f, 1.0f);
            if (index < 10)
            {
                _allButtons[index].transform.SetAsLastSibling();
            }
        }

        // 버튼 위에서 조준점 벗어났을 때 호출되는 함수
        public void OnPointerExitButton(int index)
        {
            // 버튼 축소        
            iTween.ScaleTo(_allButtons[index].gameObject, Vector3.one, 1.0f);
            //이펙트 적용
            _numEffects[index].gameObject.SetActive(false);
        }

        public void OnClickCompleteButton()
        {
            //_confirmBG.SetActive(true);        
            SendLoginInfo();
            log.Debug("IsCheck Test");
            //로그인 창 스케일 0 0 0        
            _soundBtn1.GetComponent<AudioSource>().Play();
        }

        //0809 로그인 간소화 작업 대체(게스트 버튼)
        public void OnClickGuestButton()
        {
            for (int i = 0; i < go_InputObjs.Length; i++)
            {
                go_InputObjs[i].GetComponentInChildren<Text>().text = "1";
            }

            OnClickCompleteButton();
        }

        private void SendLoginInfo()
        {
            // 마지막 자리 번호까지 다 눌렀는지 확인
            if (go_InputObjs[go_InputObjs.Length - 1].GetComponentInChildren<Text>().text != "")
            {
                // 확인창 끄기
                //_confirmBG.SetActive(false);

                // 버튼 입력 끄기
                SetButtonsInteractable(false);

                string studentId = string.Empty;

                for (int i = 0; i < go_InputObjs.Length; ++i)
                {
                    studentId += go_InputObjs[i].GetComponentInChildren<Text>().text;
                }

                // 여기서 파일 생성
                FileStream file;
                if (Application.platform == RuntimePlatform.Android)
                {
                    file = new FileStream(_strPath + "Id", FileMode.Create, FileAccess.Write);
                }
                else
                {
                    file = new FileStream("./Id", FileMode.Create, FileAccess.Write);
                }
                StreamWriter sw = new StreamWriter(file);
                sw.WriteLine(studentId);
                sw.Close();
                file.Close();

                // 확인창 Id 텍스트에 학번 입력
                _userIdText.text = studentId;

                _msgText.text = "로그인 중입니다. 잠시만 기다려 주세요.";

                if (null != _co)
                {
                    StopCoroutine(_co);
                }
                _co = StartCoroutine(this.ReLoginConfirmCoroutine());
            }
        }

        public void OnClickCancelButton()
        {
            Debug.Log("취소 버튼 클릭");
            _confirmBGObj.SetActive(false);
            SetButtonsInteractable(true);



            // 'Login' 파일 생성(내용은 'NO')
            FileStream file;
            if (Application.platform == RuntimePlatform.Android)
            {
                file = new FileStream(_strPath + "Login", FileMode.Create, FileAccess.Write);
            }
            else
            {
                file = new FileStream("./Login", FileMode.Create, FileAccess.Write);
            }
            StreamWriter sw = new StreamWriter(file);
            sw.WriteLine("NO");
            sw.Close();
            file.Close();
            //로그인창 스케일 원상복귀
            _loginBGObj.transform.localScale = _myScale;

            //파일이 존재한다면 and 내용이 ok라면 로그발생
            /*
            FileStream readFile = new FileStream(_strPath + "Login", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(readFile);

            SLog.Debug("Login file " + sr.ReadToEnd());
            log.Debug("Login file " + sr.ReadToEnd());

            _msgText.text = sr.ReadToEnd();

            sr.Close();
            readFile.Close();
            */
            _soundBtn1.GetComponent<AudioSource>().Play();

        }

        public void OnClickConfirmButton()
        {
            // 'Login' 파일 생성(내용은 'OK')
            FileStream file;
            if (Application.platform == RuntimePlatform.Android)
            {
                file = new FileStream(_strPath + "Login", FileMode.Create, FileAccess.Write);
            }
            else
            {
                file = new FileStream("./Login", FileMode.Create, FileAccess.Write);
            }
            if (file == null)
            {
                _msgText.text = "Login 파일이 없습니다.(OK)";
            }
            StreamWriter sw = new StreamWriter(file);
            sw.WriteLine("OK");
            sw.Close();
            file.Close();

            //파일이 존재한다면 and 내용이 ok라면 로그발생
            /*
            FileStream readFile = new FileStream(_strPath + "Login", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(readFile);

            SLog.Debug("Login file " + sr.ReadToEnd());
            log.Debug("Login file " + sr.ReadToEnd());

            _msgText.text = sr.ReadToEnd();

            sr.Close();
            readFile.Close();
            */
            _soundBtn1.GetComponent<AudioSource>().Play();
        }

        private void SetButtonsInteractable(bool isEnable)
        {
            for (int i = 0; i < _numButtons.Length; ++i)
            {
                _numButtons[i].interactable = isEnable;
            }

            _deleteButton.interactable = isEnable;
            _initializeButton.interactable = isEnable;
            _nextButton.interactable = isEnable;
        }

        private IEnumerator ReLoginConfirmCoroutine()
        {
            WaitForSeconds checkDelay = new WaitForSeconds(0.5f);

            int cnt = 0;

            while (!File.Exists(_strPath + "ReLogin") && !File.Exists(_strPath + "IdName") && (20 > cnt))
            {
                yield return checkDelay;
                ++cnt;
            }

            if (File.Exists(_strPath + "ReLogin") || (20 <= cnt))
            {
                _msgText.text = "로그인에 실패하였습니다. 다시 입력해 주세요.";

                // 파일 삭제
                File.Delete(_strPath + "Id");
                File.Delete(_strPath + "ReLogin");

                SetButtonsInteractable(true);
            }
            else if (File.Exists(_strPath + "IdName"))
            {
                //2018-08-07 14:11 로그인 해서 Id 파일 생성되면 바로 넘어가게 Agent 쪽에서 수정하기로 함

                //string path = _strPath + @"IdName";
                //string textValue = File.ReadAllText(path);

                //_userNameText.text = textValue;

                // 입력된 계정 맞는지 확인하는 팝업창
                //_confirmBGObj.SetActive(true);
            }
        }

        private IEnumerator CheckOKFileCoroutine()
        {
            WaitForSeconds wfs = new WaitForSeconds(0.1f);
            string path = _strPath + @"Login";
            string textValue = string.Empty;

            while (true)
            {
                // 해당 경로에 Login 파일이 있는지 확인
                if (File.Exists(_strPath + "Login"))
                {
                    // 파일 내용 확인용 변수 초기화
                    textValue = string.Empty;

                    // Login 파일 내용 읽어오기
                    textValue = File.ReadAllText(path);

                    // Login 파일 내용이 OK 일 경우
                    if (textValue.Equals("OK"))
                    {
                        // 앱 종료
                        Application.Quit();
                    }                    
                }

                // 0.1 초 대기
                yield return wfs;
            }
        }
    }
}